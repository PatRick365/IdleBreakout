#pragma once

#include "uibase.h"


class UiHud
{
public:
    UiHud();

    tgui::Panel::Ptr  panelHud;

    tgui::Label::Ptr  labelMoney;
    tgui::Button::Ptr buttonBuyBall;
    tgui::Button::Ptr buttonBuySplatter;
    tgui::Button::Ptr buttonBuySniper;

    tgui::Label::Ptr labelDebug;

    tgui::Button::Ptr buttonSettings;
    tgui::Button::Ptr buttonGame;

};
