#include "rowballstore.h"

#include <fmt/format.h>

/// just here to make map subscript operator work
RowBallStore::RowBallStore() = default;

RowBallStore::RowBallStore(Ball::Type ballType)
    : ballType(ballType)
{
    picBall = tgui::Picture::create();
    labelBall = tgui::Label::create("ball");
    buttonBallBuy = tgui::Button::create("buy\n100$");
    buttonBallSell = tgui::Button::create("sell\n100$");

    for (const auto upgrade : ConstBall::mapBallDataConst.at(ballType).validUpgrades)
    {
        mapButtonsUpgrades[upgrade] = tgui::Button::create("placeholder");
    }
}

//bool RowBallStore::needsUpdate()
//{
//    if (m_needsUpdate)
//    {
//        m_needsUpdate = false;
//        return true;
//    }
//    return false;
//}

void RowBallStore::setUnlocked(bool unlocked)
{
    if (unlocked != m_available)
    {
        picBall->setVisible(unlocked);
        labelBall->setVisible(unlocked);
        buttonBallBuy->setVisible(unlocked);

        m_available = unlocked;

        if (!unlocked)
            setGotBall(false);
    }
}

void RowBallStore::setGotBall(bool gotBall)
{
    if (gotBall != m_gotBall)
    {
//        picBall->setVisible(gotBall);
//        labelBall->setVisible(gotBall);
//        buttonBallBuy->setVisible(gotBall);
        buttonBallSell->setVisible(gotBall);

        for (const auto &butt : mapButtonsUpgrades)
            butt.second->setVisible(gotBall);

        m_gotBall = gotBall;
    }
}
