#pragma once

#include <TGUI/TGUI.hpp>


class UiSettingsMenu
{
public:
    UiSettingsMenu();

    tgui::ChildWindow::Ptr window;
    tgui::Tabs::Ptr        tabs;

/// VIDEO
    tgui::CheckBox::Ptr checkBoxFullScreen;
    tgui::ComboBox::Ptr comboBoxResolution;
    tgui::CheckBox::Ptr checkBoxVsync;
    tgui::Slider::Ptr   sliderMaxFramerate;
    tgui::Label::Ptr    labelMaxFramerate;


/// DEBUG
    tgui::Button::Ptr   buttonClearBalls;

    tgui::Button::Ptr   buttonNoMoney;
    tgui::Button::Ptr   buttonGetMoney;
    tgui::Button::Ptr   buttonUnlockAll;

    tgui::Button::Ptr   buttonQuit;

    tgui::Button::Ptr   buttonCancel;
    tgui::Button::Ptr   buttonApply;
    tgui::Button::Ptr   buttonOk;



};
