#pragma once

#include "../entities/ball.h"

#include <TGUI/TGUI.hpp>

#include <unordered_map>

class RowBallStore
{
public:
    RowBallStore();
    explicit RowBallStore(Ball::Type ballType);

//    void flagForUpdate() { m_needsUpdate = true; }
//    bool needsUpdate();

    void setGotBall(bool gotBall);
    void setUnlocked(bool unlocked);

    Ball::Type ballType = Ball::Type::eInvalid;

    tgui::Picture::Ptr  picBall;
    tgui::Label::Ptr    labelBall;
    tgui::Button::Ptr   buttonBallBuy;
    tgui::Button::Ptr   buttonBallSell;

    std::unordered_map<Ball::Upgrade, tgui::Button::Ptr> mapButtonsUpgrades;


private:
    bool m_available = true;
    bool m_gotBall = true;
    bool m_needsUpdate = true;

};
