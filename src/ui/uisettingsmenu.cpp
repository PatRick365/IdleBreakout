#include "uisettingsmenu.h"

#include "../screenlayout.h"

#include <fmt/format.h>


UiSettingsMenu::UiSettingsMenu()
{
    /// WINDOW
        window = tgui::ChildWindow::create("SettingsWindow");
        window->setPosition(ConstScreenLayout::screenWidth / 8, ConstScreenLayout::screenHeight / 12);
        window->setSize(ConstScreenLayout::screenWidth / 3, ConstScreenLayout::screenHeight / 3);
        window->setTitle("Settings Menu");
        window->setVisible(false);

    /// TABS
        tabs = tgui::Tabs::create();
        tabs->setTabHeight(30);
        tabs->setPosition(0, 0);
        tabs->add("Game");
        tabs->add("Audio");
        tabs->add("Video");
        tabs->add("Debug");
        tabs->select("Game");
        window->add(tabs);

    /// PANELS
        tgui::Panel::Ptr panelGame = tgui::Panel::create();
        panelGame->setSize({ "100%","100%" });
        panelGame->setPosition(tabs->getPosition().x, tabs->getPosition().y + 30);
        window->add(panelGame, "Game");

        tgui::Panel::Ptr panelAudio = tgui::Panel::copy(panelGame);
        panelAudio->setVisible(false);
        window->add(panelAudio, "Audio");

        tgui::Panel::Ptr panelVideo = tgui::Panel::copy(panelGame);
        panelVideo->setVisible(false);
        window->add(panelVideo, "Video");

        tgui::Panel::Ptr panelDebug = tgui::Panel::copy(panelGame);
        panelDebug->setVisible(false);
        window->add(panelDebug, "Debug");

    /// PANEL GRIDS
        auto gridGame = tgui::Grid::create();
        gridGame->setSize("80%", "60%");
        gridGame->setPosition("10%", "10%");
        panelGame->add(gridGame);

        auto gridAudio = tgui::Grid::copy(gridGame);
        panelAudio->add(gridAudio);

        auto gridVideo = tgui::Grid::copy(gridGame);
        panelVideo->add(gridVideo);

        auto gridDebug = tgui::Grid::copy(gridGame);
        panelDebug->add(gridDebug);

        /// GAME PANEL
        {
        }
        /// AUDIO PANEL
        {
        }
        /// VIDEO PANEL
        {
            /// FULL SCREEN
            {
                auto labelCheckBoxFullScreen = tgui::Label::create("Full screen");
                gridVideo->addWidget(labelCheckBoxFullScreen, 0, 0);
                gridVideo->add(labelCheckBoxFullScreen);

                checkBoxFullScreen = tgui::CheckBox::create();
                gridVideo->addWidget(checkBoxFullScreen, 0, 1);
                gridVideo->add(checkBoxFullScreen);
            }
            /// RESOLUTION
            {
                auto labelResolution = tgui::Label::create("Resolution");
                gridVideo->addWidget(labelResolution, 1, 0);
                gridVideo->add(labelResolution);

                comboBoxResolution = tgui::ComboBox::create();
                gridVideo->addWidget(comboBoxResolution, 1, 1);
                gridVideo->add(comboBoxResolution);
            }
            /// VSYNC
            {
                auto labelVsync = tgui::Label::create("Vsync");
                gridVideo->addWidget(labelVsync, 2, 0);
                gridVideo->add(labelVsync);

                checkBoxVsync = tgui::CheckBox::create();
                gridVideo->addWidget(checkBoxVsync, 2, 1);
                gridVideo->add(checkBoxVsync);


            }
            /// FRAMERATE
            {
                auto labelFramerate = tgui::Label::create("Framerate");
                gridVideo->addWidget(labelFramerate, 3, 0);
                gridVideo->add(labelFramerate);

                sliderMaxFramerate = tgui::Slider::create(0, 240);
                gridVideo->addWidget(sliderMaxFramerate, 3, 1);

                labelMaxFramerate = tgui::Label::create("60?");
                gridVideo->addWidget(labelMaxFramerate, 3, 3);
                gridVideo->add(labelMaxFramerate);

                // add on top // does nothing ...
                gridVideo->add(sliderMaxFramerate);
            }

            /*auto labelMaxFramerate = tgui::Label::create("Max framerate");
            grid->addWidget(labelMaxFramerate, 3, 0);
            grid->add(labelMaxFramerate);
            sliderMaxFramerate = tgui::Slider::create(3);
            grid->addWidget(sliderMaxFramerate, 3, 1);
            grid->add(sliderMaxFramerate);*/

        }
        /// DEBUG PANEL
        {
            buttonClearBalls = tgui::Button::create("Clear Balls");
            gridDebug->addWidget(buttonClearBalls, 0, 0);
            gridDebug->add(buttonClearBalls);

            buttonNoMoney = tgui::Button::create("No money");
            gridDebug->addWidget(buttonNoMoney, 1, 0);
            gridDebug->add(buttonNoMoney);

            buttonGetMoney = tgui::Button::create("Get money");
            gridDebug->addWidget(buttonGetMoney, 1, 1);
            gridDebug->add(buttonGetMoney);

            buttonUnlockAll = tgui::Button::create("Unlock all");
            gridDebug->addWidget(buttonUnlockAll, 2, 1);
            gridDebug->add(buttonUnlockAll);


        }


    /// CONTROL BUTTONS

        //buttonQuit = tgui::Button::create("Quit");


        auto layoutControl = tgui::HorizontalLayout::create();
        layoutControl->setPosition(0, window->getSize().y - 30);
        layoutControl->setSize("100%", 30);
        window->add(layoutControl);

    //    buttonCancel = tgui::Button::create("Cancel");
    //    layoutControl->add(buttonCancel);
    //    buttonApply = tgui::Button::create("Apply");
    //    layoutControl->add(buttonApply);
        buttonOk = tgui::Button::create("Ok");
        layoutControl->add(buttonOk);
}


/*
        /// @todo: at some point these need to be loaded from settings, maybe a function in here, in here called once, then in uisystem on change

        auto resolutions = sf::VideoMode::getFullscreenModes();

        /// @todo: maybe load every time opened. screen could change
        for (unsigned int i = 0; i < resolutions.size(); ++i)
        {
            if (resolutions[i].bitsPerPixel == 32)
                comboBoxResolution->addItem(fmt::format("{}x{}", resolutions[i].width, resolutions[i].height), std::to_string(i));
        }
        comboBoxResolution->setSelectedItemById("0");

*/
