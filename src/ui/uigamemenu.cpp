#include "uigamemenu.h"

#include "../screenlayout.h"
#include "../utility/quickmath.h"

#include <fmt/format.h>

UiGameMenu::UiGameMenu()
{
    /// WINDOW
        window = tgui::ChildWindow::create("GameWindow");
        window->setPosition(ConstScreenLayout::screenWidth / 8, ConstScreenLayout::screenHeight / 12);
        window->setSize(ConstScreenLayout::screenWidth / 2, ConstScreenLayout::screenHeight / 3);
        window->setTitle("Game Menu");
        window->setVisible(false);

    /// TABS
        tabs = tgui::Tabs::create();
        tabs->setTabHeight(30);
        tabs->setPosition(0, 0);
        tabs->add("Balls");
        //    tabs->add("Audio");
        //    tabs->add("Video");
        tabs->select("Balls");
        window->add(tabs);

    /// PANELS
        tgui::Panel::Ptr panelBalls = tgui::Panel::create();
        panelBalls->setSize({ "100%","100%" });
        panelBalls->setPosition(tabs->getPosition().x, tabs->getPosition().y + 30);
        window->add(panelBalls, "Balls");

    //    tgui::Panel::Ptr panelAudio = tgui::Panel::copy(panelGame);
    //    panelAudio->setVisible(false);
    //    window->add(panelAudio, "Audio");

    //    tgui::Panel::Ptr panelVideo = tgui::Panel::copy(panelGame);
    //    panelVideo->setVisible(false);
    //    window->add(panelVideo, "Video");

        auto gridBalls = tgui::Grid::create();
        //gridBalls->setSize("100%", "100%");
        gridBalls->setSize("92%", "60%");
        gridBalls->setPosition("3%", "3%");
        panelBalls->add(gridBalls);

    //    auto gridAudio = tgui::Grid::copy(gridGame);
    //    panelAudio->add(gridAudio);

    //    auto gridVideo = tgui::Grid::copy(gridGame);
    //    panelVideo->add(gridVideo);


        /// GAME PANEL
        {
            for (unsigned int i = 0; i < listEnumBallType.size(); ++i)
            {
                mapRowBallStore[listEnumBallType[i]] = RowBallStore(listEnumBallType[i]);

                gridBalls->addWidget(mapRowBallStore[listEnumBallType[i]].picBall, i, 0);
                gridBalls->add(mapRowBallStore[listEnumBallType[i]].picBall);
                mapRowBallStore[listEnumBallType[i]].labelBall->setText(ConstBall::mapBallDataConst.at(listEnumBallType[i]).name);
                gridBalls->addWidget(mapRowBallStore[listEnumBallType[i]].labelBall, i, 1);
                gridBalls->add(mapRowBallStore[listEnumBallType[i]].labelBall);
                gridBalls->addWidget(mapRowBallStore[listEnumBallType[i]].buttonBallBuy, i, 2);
                gridBalls->add(mapRowBallStore[listEnumBallType[i]].buttonBallBuy);
                gridBalls->addWidget(mapRowBallStore[listEnumBallType[i]].buttonBallSell, i, 3);
                gridBalls->add(mapRowBallStore[listEnumBallType[i]].buttonBallSell);

                unsigned int j = 0;
                for (const auto &butt : mapRowBallStore[listEnumBallType[i]].mapButtonsUpgrades)
                {
                    gridBalls->add(butt.second);
                    gridBalls->addWidget(butt.second, i, 4 + j++);

                }


    //            gridBalls->addWidget(mapRowBallStore[listEnumBallType[i]].buttonBallUpgradeSpeed, i, 4);
    //            gridBalls->add(mapRowBallStore[listEnumBallType[i]].buttonBallUpgradeSpeed);
    //            gridBalls->addWidget(mapRowBallStore[listEnumBallType[i]].buttonBallUpgradeDamage, i, 5);
    //            gridBalls->add(mapRowBallStore[listEnumBallType[i]].buttonBallUpgradeDamage);
            }

//            sf::Vector2f maxSizePicBall;
//            sf::Vector2f maxSizeLabelBall;
//            sf::Vector2f maxSizeButtonBallBuy;
//            sf::Vector2f maxSizeButtonBallSell;
//            for (unsigned int i = 0; i < listEnumBallType.size(); ++i)
//            {
//                maxSizePicBall = QM::max(mapRowBallStore[listEnumBallType[i]].picBall->getSize(), maxSizePicBall);
//                maxSizeLabelBall = QM::max(mapRowBallStore[listEnumBallType[i]].labelBall->getSize(), maxSizeLabelBall);
//                maxSizeButtonBallBuy = QM::max(mapRowBallStore[listEnumBallType[i]].buttonBallBuy->getSize(), maxSizeButtonBallBuy);
//                maxSizeButtonBallSell = QM::max(mapRowBallStore[listEnumBallType[i]].buttonBallSell->getSize(), maxSizeButtonBallSell);
//            }
//            for (unsigned int i = 0; i < listEnumBallType.size(); ++i)
//            {
//                mapRowBallStore[listEnumBallType[i]].picBall->setSize(maxSizePicBall);
//                mapRowBallStore[listEnumBallType[i]].labelBall->setSize(maxSizeLabelBall);
//                mapRowBallStore[listEnumBallType[i]].buttonBallBuy->setSize(maxSizeButtonBallBuy);
//                mapRowBallStore[listEnumBallType[i]].buttonBallSell->setSize(maxSizeButtonBallSell);
//            }

//            sf::Vector2f maxSize;

//            for (const auto ballType : listEnumBallType)
//            {
//                maxSize = QM::max(mapRowBallStore[ballType].picBall->getSize(), maxSize);
//                maxSize = QM::max(mapRowBallStore[ballType].labelBall->getSize(), maxSize);
//                maxSize = QM::max(mapRowBallStore[ballType].buttonBallBuy->getSize(), maxSize);
//                maxSize = QM::max(mapRowBallStore[ballType].buttonBallSell->getSize(), maxSize);
//            }
//            for (const auto ballType : listEnumBallType)
//            {
//                mapRowBallStore[ballType].picBall->setSize(maxSize);
//                mapRowBallStore[ballType].labelBall->setSize(maxSize);
//                mapRowBallStore[ballType].buttonBallBuy->setSize(maxSize);
//                mapRowBallStore[ballType].buttonBallSell->setSize(maxSize);
//            }


        }
        /// AUDIO PANEL
        {
        }
        /// VIDEO PANEL
        {
        }

        /// CONTROL BUTTONS
        auto layoutControl = tgui::HorizontalLayout::create();
        layoutControl->setPosition(0, window->getSize().y - 30);
        layoutControl->setSize("100%", 30);
        window->add(layoutControl);

    //    buttonCancel = tgui::Button::create("Cancel");
    //    layoutControl->add(buttonCancel);
    //    buttonApply = tgui::Button::create("Apply");
    //    layoutControl->add(buttonApply);
        buttonOk = tgui::Button::create("Ok");
        layoutControl->add(buttonOk);
}
