#include "uihud.h"

#include "../screenlayout.h"

#include <fmt/format.h>

UiHud::UiHud()
{
    panelHud = tgui::Panel::create();
    //panelHud->setSize({ConstScreenLayout::uiWidth, ConstScreenLayout::hudHeight});
    panelHud->setSize({ConstScreenLayout::screenWidth, ConstScreenLayout::playBorderY - 30}); /// @todo why mismatch ?

    auto gridHud= tgui::Grid::create();
    gridHud->setPosition("5%", "5%");
    gridHud->setSize("90%", "90%");
    panelHud->add(gridHud);

    labelMoney = tgui::Label::create();
    labelMoney->setTextSize(20);
    gridHud->addWidget(labelMoney, 0, 1);
    gridHud->add(labelMoney);

    labelDebug = tgui::Label::create();
    labelDebug->setTextSize(20);
    gridHud->addWidget(labelDebug, 1, 1);
    gridHud->add(labelDebug);

    buttonBuyBall = tgui::Button::create();
    gridHud->addWidget(buttonBuyBall, 0, 0);
    gridHud->add(buttonBuyBall);

    buttonBuySplatter = tgui::Button::create();
    gridHud->addWidget(buttonBuySplatter, 1, 0);
    gridHud->add(buttonBuySplatter);

    buttonBuySniper = tgui::Button::create();
    gridHud->addWidget(buttonBuySniper, 2, 0);
    gridHud->add(buttonBuySniper);

    buttonSettings = tgui::Button::create("Sett");
    gridHud->addWidget(buttonSettings, 0, 2);
    gridHud->add(buttonSettings);

    buttonGame = tgui::Button::create("Game");
    gridHud->addWidget(buttonGame, 2, 1);
    gridHud->add(buttonGame);

}
