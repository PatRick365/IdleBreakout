#pragma once

#include <TGUI/TGUI.hpp>

#include <memory>

class UiBase
{
public:
    UiBase();

    virtual ~UiBase();

    virtual void init(tgui::Gui &tgui) = 0;

};
