#pragma once

#include "../entities/ball.h"
#include "rowballstore.h"


#include <unordered_map>


class UiGameMenu
{
public:
    UiGameMenu();

    tgui::ChildWindow::Ptr window;
    tgui::Tabs::Ptr        tabs;

    std::unordered_map<Ball::Type, RowBallStore> mapRowBallStore;

    tgui::Button::Ptr   buttonCancel;
    tgui::Button::Ptr   buttonApply;
    tgui::Button::Ptr   buttonOk;

};
