#include "datafactory.h"

#include "filepaths.h"

#include "utility/dataserialization.hpp"

#include <fmt/format.h>

#include <memory>


DataFactory::DataFactory() = default;


std::unique_ptr<DataRef> DataFactory::makeData()
{
    // m_debugDatam
    // m_gameStateData


    settingsData = DataSerialization::loadData<SettingsData>(FilePaths::dirSave);

    fmt::print("res: {},{}\n", settingsData.graphics.resolutionX, settingsData.graphics.resolutionY);

    outputData.renderWindow = std::make_unique<sf::RenderWindow>();
//    outputData.renderWindow->create(sf::VideoMode(1920, 1080),
//                                    "Idle", settingsData.graphics.isFullscreenEnabled ? sf::Style::Fullscreen : sf::Style::Default);

    outputData.renderWindow->create(sf::VideoMode(settingsData.graphics.resolutionX, settingsData.graphics.resolutionY),
                                    "Idle", settingsData.graphics.isFullscreenEnabled ? sf::Style::Fullscreen : sf::Style::Default);

    /// @todo evaluate if this is best
//    outputData.renderWindow->setVerticalSyncEnabled(settingsData.graphics.isVsyncEnabled);
//    outputData.renderWindow->setFramerateLimit(settingsData.graphics.maxFps);

//    graphics.isFullscreenEnabled
//    graphics.resolutionX;
//    graphics.resolutionY;


    uiData.tgui = std::make_unique<tgui::Gui>(*outputData.renderWindow);
    uiData.initWidgets();
    entityData = DataSerialization::loadData<EntityData>(FilePaths::dirSave);

    //outputData = OutputData(renderWindow, uiManager);
    // data.event
    playerData = DataSerialization::loadData<PlayerData>(FilePaths::dirSave);


    return std::make_unique<DataRef>(DataRef { debugData, settingsData, gameStateData, calculationData, uiData, entityData, outputData, eventData, playerData });
}

