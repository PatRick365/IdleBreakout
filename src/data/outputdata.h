#pragma once

#include "database.h"

#include <SFML/Graphics/RenderWindow.hpp>

#include <memory>


class OutputData : public DataBase
{
public:
    static const std::string className;

    OutputData();

    std::unique_ptr<sf::RenderWindow> renderWindow;
    //soundmanager



};
