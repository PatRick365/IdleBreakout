#include "eventdata.h"


const std::string EventData::className = "EventData";

EventData::EventData()
    : DataBase("EventData")
{}

bool EventData::hasMouseEvent()
{
    return (mouseMoved || (!pressedMouseButtons.empty()) || (!releasedMouseButtons.empty()));
}
