#pragma once

#include "database.h"

#include "../types.h"

#include "../entities/ball.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/unordered_map.hpp>

#include <chrono>
#include <unordered_map>


struct BallDataMod
{
    int count = 0;
    bool unlocked = false;
    std::unordered_map<Ball::Upgrade, int> upgrades;

private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &archive, const unsigned int /*version*/)
    {
        archive & count;
        archive & unlocked;
        archive & upgrades;
    }
};

class PlayerData : public DataBase
{
public:
    static const std::string className;

    PlayerData();

    money_t money = 0;

    int currentLevel = 0;

    std::unordered_map<Ball::Type, BallDataMod> mapBallDataMod;


private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &archive, const unsigned int /*version*/)
    {
        archive & money;
        archive & currentLevel;
        archive & mapBallDataMod;
    }
};
