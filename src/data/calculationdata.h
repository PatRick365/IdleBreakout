#pragma once

#include "database.h"

#include "../types.h"

#include "../entities/ball.h"

#include <unordered_map>


class CalculationData : public DataBase
{
public:
    static const std::string className;

    CalculationData();

    std::unordered_map<Ball::Type, float> ballSpeed;
    std::unordered_map<Ball::Type, float> ballRange;
    std::unordered_map<Ball::Type, money_t> ballBuyPrice;
    std::unordered_map<Ball::Type, int> ballDamage;

    std::unordered_map<Ball::Type, money_t> ballSellPrice;

    std::unordered_map<Ball::Type, std::unordered_map<Ball::Upgrade, money_t>> ballPriceUpgrade;


};
