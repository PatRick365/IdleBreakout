#pragma once

#include "database.h"

#include <SFML/Graphics/Texture.hpp>


class GraphicsData : public DataBase
{
public:
    static const std::string className;

    GraphicsData();


    sf::Texture m;
};
