#pragma once

#include "database.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>


class SettingsData : public DataBase
{
public:
    static const std::string className;

    class GameplaySettings
    {
    public:
        GameplaySettings();
    };

    class GraphicsSettings
    {
    public:
        GraphicsSettings();

        bool isFullscreenEnabled = false;
        bool isVsyncEnabled = false;
        unsigned int resolutionX = 1920;
        unsigned int resolutionY = 1080;
        unsigned int maxFps = 0; // 0 == unlocked

    };

    class AudioSettings
    {
    public:
        AudioSettings();
    };


    SettingsData();

    GameplaySettings gameplay;
    GraphicsSettings graphics;
    AudioSettings audio;

private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &archive, const unsigned int /*version*/)
    {
        archive & graphics.isFullscreenEnabled;
        archive & graphics.isVsyncEnabled;
        archive & graphics.resolutionX;
        archive & graphics.resolutionY;
        archive & graphics.maxFps;
    }
};
