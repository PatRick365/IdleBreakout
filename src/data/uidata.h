#pragma once

#include "database.h"

#include "../ui/uihud.h"
#include "../ui/uigamemenu.h"
#include "../ui/uisettingsmenu.h"

#include <TGUI/Gui.hpp>

#include <memory>

class UiData : public DataBase
{
public:
    static const std::string className;

    UiData();

    void initWidgets();

    std::unique_ptr<tgui::Gui> tgui;

    UiHud menuHud;
    UiGameMenu menuGame;
    UiSettingsMenu menuSettings;

};
