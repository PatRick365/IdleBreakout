#pragma once

#include <string>


class DataBase
{
public:
    DataBase(const std::string &name);


    std::string name() const;

private:
    std::string m_name;
};
