#pragma once

#include "database.h"

#include <unordered_set>
#include <memory>

#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/System/Vector2.hpp>


class EventData : public DataBase
{
public:
    static const std::string className;

    EventData();

    std::unordered_set<sf::Keyboard::Key> pressedKeys;
    std::unordered_set<sf::Keyboard::Key> releasedKeys;

    sf::Vector2f mousePos;

    std::unordered_set<sf::Mouse::Button> pressedMouseButtons;
    std::unordered_set<sf::Mouse::Button> releasedMouseButtons;

    bool mouseMoved{false};

    bool hasMouseEvent();


    /// @todo: f make_timer, holds weak_pointer here, gets updated by eventsystem, eventsystem deletes when done.
    /// or maybe dont return object but pass a bool flag to be set

};
