#pragma once

#include "database.h"

#include "../entities/ball.h"
#include "../entities/brick.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>

#include "../screenlayout.h"

#include <memory>
#include <vector>


namespace ConstBrick
{
static constexpr unsigned int width = 80;
static constexpr unsigned int height = 36;

static constexpr unsigned int levelBrickWidth = ConstScreenLayout::playBorderWidth / width;     // ==
static constexpr unsigned int levelBrickHeight = ConstScreenLayout::playBorderHeight / height;  // == 25
}

class EntityData : public DataBase
{
public:
    static const std::string className;

    EntityData();

    std::vector<Ball> balls;

    std::vector<Brick> bricks;
    std::vector<sf::Vector2f> spawnPoints;  /// @todo: save or init somehow

    std::unordered_map<Ball::Type, sf::Sprite> mapBallSprite;


//private:
//    sf::Texture m_textureBall;
//    sf::Texture m_textureSplatter;
//    sf::Texture m_textureSniper;

    //temp
    sf::Sprite spriteBall;


private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &archive, const unsigned int /*version*/)
    {
        archive & balls;
        archive & bricks;
        archive & spawnPoints;
    }
};
