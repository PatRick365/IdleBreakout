#pragma once

#include "database.h"


class GameStateData : public DataBase
{
public:
    static const std::string className;

    enum class State
    {
        GameLoad,
        LevelLoad,
        Running,
        Paused,
        LevelEnd
    };


public:
    GameStateData();

    State state  = State::Running;


};

