#pragma once


class DebugData;
class SettingsData;
class GameStateData;
class CalculationData;
class UiData;
class EntityData;
class EventData;
class OutputData;
class PlayerData;

struct DataRef
{
    DebugData &debug;
    SettingsData &settings;
    GameStateData &gameState;
    CalculationData &calculations;
    UiData &ui;
    EntityData &entity;
    OutputData &output;
    EventData &event;
    PlayerData &player;

};
