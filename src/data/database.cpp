#include "database.h"


DataBase::DataBase(const std::string &name)
    : m_name(name)
{}

std::string DataBase::name() const
{
    return m_name;
}
