#include "settingsdata.h"

#include <SFML/Window/VideoMode.hpp>


const std::string SettingsData::className = "SettingsData";

SettingsData::SettingsData()
    : DataBase("SettingsData")
{}

SettingsData::GameplaySettings::GameplaySettings() = default;
SettingsData::GraphicsSettings::GraphicsSettings() = default;
SettingsData::AudioSettings::AudioSettings() = default;
