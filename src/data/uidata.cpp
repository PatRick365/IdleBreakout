#include "uidata.h"


const std::string UiData::className = "UiData";

UiData::UiData()
    : DataBase("UiData")
{}

void onSettingsMenuTabSelected(tgui::Gui& tgui, const std::string &selectedTab);
void onGameMenuTabSelected(tgui::Gui& tgui, const std::string &selectedTab);

void UiData::initWidgets()
{
/// ADD UI MODULES
    tgui->add(menuHud.panelHud);
    tgui->add(menuGame.window);
    tgui->add(menuSettings.window);


/// SETTINGS WINDOW
    menuSettings.tabs->connect("TabSelected", onSettingsMenuTabSelected, std::ref(*tgui));

    // do not destroy window on close, but hide it
    menuSettings.window->connect("closed", [&] {
        menuSettings.window->setVisible(false);
    });

    menuSettings.buttonOk->connect("pressed", [&]() {
        menuSettings.window->setVisible(false);
    });


/// GAME WINDOW
    menuGame.tabs->connect("TabSelected", onGameMenuTabSelected, std::ref(*tgui));

    // do not destroy window on close, but hide it
    menuGame.window->connect("closed", [&] {
        menuGame.window->setVisible(false);
    });

    menuHud.buttonSettings->connect("pressed", [&]() {
        bool show = !menuSettings.window->isVisible();
        menuSettings.window->setVisible(show);
        if (show)
            menuSettings.window->moveToFront();
    });

    menuGame.buttonOk->connect("pressed", [&]() {
        bool show = !menuGame.window->isVisible();
        menuGame.window->setVisible(show);
        if (show)
            menuGame.window->moveToFront();
    });


/// HUD
    menuHud.buttonGame->connect("pressed", [&]() {
        menuGame.window->setVisible(!menuGame.window->isVisible());
    });


}


void onSettingsMenuTabSelected(tgui::Gui& tgui, const std::string &selectedTab)
{
    if (selectedTab == "Game")
    {
        tgui.get("Game")->setVisible(true);
        tgui.get("Audio")->setVisible(false);
        tgui.get("Video")->setVisible(false);
        tgui.get("Debug")->setVisible(false);
    }
    else if (selectedTab == "Audio")
    {
        tgui.get("Game")->setVisible(false);
        tgui.get("Audio")->setVisible(true);
        tgui.get("Video")->setVisible(false);
        tgui.get("Debug")->setVisible(false);
    }
    else if (selectedTab == "Video")
    {
        tgui.get("Game")->setVisible(false);
        tgui.get("Audio")->setVisible(false);
        tgui.get("Video")->setVisible(true);
        tgui.get("Debug")->setVisible(false);
    }
    else if (selectedTab == "Debug")
    {
        tgui.get("Game")->setVisible(false);
        tgui.get("Audio")->setVisible(false);
        tgui.get("Video")->setVisible(false);
        tgui.get("Debug")->setVisible(true);
    }
}

void onGameMenuTabSelected(tgui::Gui& tgui, const std::string &selectedTab)
{
    if (selectedTab == "Balls")
    {
        tgui.get("Balls")->setVisible(true);
        //tgui.get("Audio")->setVisible(false);
        //tgui.get("Video")->setVisible(false);
    }
//    else if (selectedTab == "Audio")
//    {
//        tgui.get("Balls")->setVisible(false);
//        tgui.get("Audio")->setVisible(true);
//        tgui.get("Video")->setVisible(false);
//    }
//    else if (selectedTab == "Video")
//    {
//        tgui.get("Balls")->setVisible(false);
//        tgui.get("Audio")->setVisible(false);
//        tgui.get("Video")->setVisible(true);
//    }
}
