#include "playerdata.h"


const std::string PlayerData::className = "PlayerData";

PlayerData::PlayerData()
    : DataBase("PlayerData")
{
    std::unordered_map<Ball::Upgrade, int> mapUpgrades;
    for (const auto upgrade : listEnumUpgrade)
        mapUpgrades[upgrade] = 1;

    mapBallDataMod = {
        { Ball::Type::eBall,        { 0, true,  mapUpgrades } },
        { Ball::Type::eSplatter,    { 0, false, mapUpgrades } },
        { Ball::Type::eSniper,      { 0, false, mapUpgrades } }
    };
}


