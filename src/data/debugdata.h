#pragma once

#include "database.h"

#include "../entities/ball.h"

#include <chrono>
#include <list>

#include <SFML/Graphics/Vertex.hpp>


class DebugData : public DataBase
{
public:
    static const std::string className;

    class Line
    {
    public:
        Line(sf::Vector2f p1, sf::Vector2f p2, sf::Color col)
            : points{p1, p2},
              col(col)
        {}

        std::array<sf::Vector2f, 2> points;
        sf::Color col;
    };


public:
    DebugData();


public:
    int frameLastFps{0};
    int frameCount{0};
    std::chrono::microseconds frameCountdown{};
    bool showFps{false};

    std::list<Line> trails;


    bool drawPath{false};


};
