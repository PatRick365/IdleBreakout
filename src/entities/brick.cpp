#include "brick.h"

#include "../utility/colors.h"

Brick::Brick() = default;

Brick::Brick(Type type, const sf::Vector2f &pos, const sf::Vector2f &size)
    : type(type),
      pos(pos),
      size(size),
      m_isDestructible(false),
      m_health(0)
{}

Brick::Brick(Type type, const sf::Vector2f &pos, const sf::Vector2f &size, int health)
    : type(type),
      pos(pos),
      size(size),
      m_isDestructible(true),
      m_health(health)
{}

/// returns damage done
int Brick::attack(int damage)
{
    if (m_isEnabled && m_isDestructible)
    {
        int damageDone = damage;
        m_health -= damage;
        if (m_health <= 0)
        {
            m_isEnabled = false;
            damageDone += m_health;
        }

//        if (damageDone < 0)
//        {
//            int i = 0;
//        }

        return damageDone;
    }
    return 0;
}

bool Brick::isEnabled() const
{
    return m_isEnabled;
}

bool Brick::isDestructible() const
{
    return m_isDestructible;
}

int Brick::health() const
{
    return m_health;
}

sf::Color Brick::color() const
{
    switch (type)
    {
    case Brick::Type::eBrick:
        return ConstColors::palette[static_cast<unsigned int>(health()) % ConstColors::colorCount];
    case Brick::Type::eBoss:
        return sf::Color::Yellow;
    case Brick::Type::eWall:
        return sf::Color::Blue;
    case Brick::Type::eInvalid:
        break;
    }
    return sf::Color::Magenta;
}

sf::Color Brick::colorOutline() const
{
    switch (type)
    {
    case Brick::Type::eBrick:
    case Brick::Type::eBoss:
        return sf::Color(66, 66, 66);
    case Brick::Type::eWall:
        return sf::Color::Cyan;
    case Brick::Type::eInvalid:
        break;
    }
    return sf::Color::Magenta;
}
