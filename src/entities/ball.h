#pragma once

#include <SFML/System/Vector2.hpp>
//#include <SFML/Graphics/CircleShape.hpp>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <unordered_map>
#include <unordered_set>


class Ball
{
public:
    enum class Type { eInvalid, eBall, eSplatter, eSniper };
    static Type getHigherBall(Type ballType);

    enum class Upgrade { eInvalid, eSpeed, eDamage, eRange };


public:
    Ball();
    Ball(Ball::Type type, const sf::Vector2f &pos, sf::Vector2f dir, float speed);


public:
    Ball::Type type;

    sf::Vector2f pos;
    sf::Vector2f dir;
    float speed = 0;


private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &archive, const unsigned int /*version*/)
    {
        archive & type;
        archive & pos;
        archive & dir;
        archive & speed;
    }
};

extern const std::array<Ball::Type, 3> listEnumBallType;
extern const std::array<Ball::Upgrade, 3> listEnumUpgrade;

struct BallDataConst
{
    std::string name;
    const float radius;
    const float baseSpeed;
    const int baseDamage;
    const float baseRange;
    const int basePrice;
    const std::vector<Ball::Upgrade> validUpgrades;
};

extern std::string toString(Ball::Upgrade upgrade);

namespace ConstBall
{
extern const std::unordered_map<Ball::Type, BallDataConst> mapBallDataConst;
}
