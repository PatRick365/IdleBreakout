// use this ?

#pragma once

#include "../entities/brick.h"

#include <vector>
#include <string>


class Level
{
public:
    explicit Level(const std::vector<std::string> &levelData);
};
