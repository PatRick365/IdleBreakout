#pragma once

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <SFML/Graphics.hpp>


class Brick
{
public:
    enum class Type { eInvalid, eBrick, eBoss, eWall };

    Brick();
    Brick(Type type, const sf::Vector2f &pos, const sf::Vector2f &size);
    Brick(Type type, const sf::Vector2f &pos, const sf::Vector2f &size, int health);

    Brick::Type type = Type::eInvalid;

    sf::Vector2f pos;
    sf::Vector2f size;

    int attack(int damage);

    bool isEnabled() const;
    bool isDestructible() const;
    int health() const;

    sf::Color color() const;
    sf::Color colorOutline() const;


private:
    bool m_isEnabled = true;
    bool m_isDestructible = false;
    int m_health = 0;


private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &archive, const unsigned int /*version*/)
    {
        archive & type;
        archive & pos;
        archive & size;
        archive & m_isEnabled;
        archive & m_isDestructible;
        archive & m_health;
    }
};
