#include "ball.h"


Ball::Type Ball::getHigherBall(Ball::Type ballType)
{
    auto it = std::find(listEnumBallType.begin(), listEnumBallType.end(), ballType);
    if (++it == listEnumBallType.end())
        return Ball::Type::eInvalid;

    return *it;
}

Ball::Ball() = default;

Ball::Ball(Type type, const sf::Vector2f &pos, sf::Vector2f dir, float speed)
    : type(type),
      pos(pos),
      dir(dir),
      speed(speed)
{}

std::string toString(Ball::Upgrade upgrade)
{
    switch (upgrade)
    {
    case Ball::Upgrade::eInvalid: return "INVALID";
    case Ball::Upgrade::eSpeed: return "Speed";
    case Ball::Upgrade::eDamage: return "Damage";
    case Ball::Upgrade::eRange: return "Range";
    }
    return "toString ERROR";
}

//std::string name;
//const float radius;
//const float baseSpeed;
//const int baseDamage;
//const float baseAttackRadius;
//const int basePrice;
//std::unordered_set<Ball::Upgrade> validUpgrades;

namespace ConstBall
{
const std::unordered_map<Ball::Type, BallDataConst> mapBallDataConst {
    //                         name         radius  base speed  base damage  base attack radius  base price  viable upgrades
    { Ball::Type::eBall,     { "Ball",      10,     0.2F,       1,           0.0F,               25,         { Ball::Upgrade::eSpeed, Ball::Upgrade::eDamage } } },
    { Ball::Type::eSplatter, { "Splatter",  12,     0.2F,       3,           30.0F,               200,       { Ball::Upgrade::eSpeed, Ball::Upgrade::eDamage, Ball::Upgrade::eRange } } },
    { Ball::Type::eSniper,   { "Sniper",    8,      0.3F,       5,           0.0F,               1500,       { Ball::Upgrade::eSpeed, Ball::Upgrade::eDamage } } }
};
}

constexpr std::array<Ball::Type, 3>    listEnumBallType = {
    Ball::Type::eBall,
    Ball::Type::eSplatter,
    Ball::Type::eSniper
};
constexpr std::array<Ball::Upgrade, 3> listEnumUpgrade = {
    Ball::Upgrade::eSpeed,
    Ball::Upgrade::eDamage,
    Ball::Upgrade::eRange
};
