#pragma once

namespace ConstScreenLayout
{

static constexpr unsigned int screenWidth = 1920;
static constexpr unsigned int screenHeight = 1080;

static constexpr unsigned int playBorderX = 80 / 2;
static constexpr unsigned int playBorderY = 36 * 5 - 20;
static constexpr unsigned int playBorderWidth = screenWidth - 80;
static constexpr unsigned int playBorderHeight = screenHeight - playBorderY;

}
