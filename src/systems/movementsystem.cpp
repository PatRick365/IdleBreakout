#include "movementsystem.h"

#include "../utility/quickmath.h"
#include "../utility/vectoroverloads.h"

#include "../data/calculationdata.h"
#include "../data/entitydata.h"
#include "../data/debugdata.h"
#include "../data/gamestatedata.h"
#include "../data/playerdata.h"
#include "../data/uidata.h"

#include <fmt/format.h>

#include <array>


using namespace std::chrono_literals;

MovementSystem::MovementSystem(DataRef &dataRef)
    : System(dataRef)
{

//    auto print = [](const char *x, const sf::Vector2f vec) {
//        //fmt::print("{}: {},{} - sum: \n", x, vec.x, vec.y, std::abs(vec.x) + std::abs(vec.y));
//        //fmt::print("{}: {:.{}f},{:.{}f} - sum: {}\n", x, vec.x, 4, vec.y, 4, std::abs(vec.x) + std::abs(vec.y));
//        fmt::print("{}: {:.{}f},{:.{}f} - l: {}\n", x, vec.x, 4, vec.y, 4, QM::vectorLength(vec));
//    };

//    for (int i = 0; i < 360; ++i)
//    {
//        print(std::to_string(i).c_str(), QM::rotate(sf::Vector2f(1, 0), i));
//    }

}

void MovementSystem::preUpdate()
{

}

void MovementSystem::update(std::chrono::microseconds timeDiff)
{
    if (data.gameState.state != GameStateData::State::Running)
        return;

    for (Ball &ball : data.entity.balls)
    {
        float dist = data.calculations.ballSpeed[ball.type] * timeDiff.count() / 1000.0F;
        calcTargetPosWithCollisions(ball, dist);
    }

}

void MovementSystem::postUpdate()
{

}

void MovementSystem::processActionEvents()
{
    /*for (const auto &action : data.event.actionEvents)
    {
        switch (action)
        {
        default:
            break;
        }
    }*/
}

void MovementSystem::calcTargetPosWithCollisions(Ball &ball, float dist)
{
    if (dist < .00001F)
        return;

    sf::Vector2f targetPos = ball.pos + ball.dir * dist;
    float ballRadius = ConstBall::mapBallDataConst.at(ball.type).radius;


    sf::Vector2f vecIntersectNearest;
    float distSquaredNearest = 0;
    bool collisionHappened = false;
    bool isHorizontal = false;
    Brick *brickCollision = nullptr;

    for (Brick &brick : data.entity.bricks)
    {
        if (!brick.isEnabled())
            continue;

        std::array<sf::Vector2f, 4> points = { brick.pos - ballRadius,                                    // pUpperLeft
                                   brick.pos + sf::Vector2f(brick.size.x + ballRadius, - ballRadius),     // pUpperRight
                                   brick.pos + brick.size + ballRadius,                                   // pLowerRight
                                   brick.pos + sf::Vector2f(-ballRadius, brick.size.y + ballRadius) };    // pLowerLeft

        // lines:                     upper             right             lower             left
        std::array<bool, 4> collisionPossible = { (ball.dir.y > 0), (ball.dir.x < 0), (ball.dir.y < 0), (ball.dir.x > 0) };

        for (unsigned int i = 0; i < 4; i ++)
        {
            if (collisionPossible[i] && QuickMath::doIntersect(ball.pos, targetPos, points[i], points[(i + 1) % 4]))
            {
                sf::Vector2f vecIntersect = QuickMath::lineLineIntersection(ball.pos, targetPos, points[i], points[(i + 1) % 4]);

                float distSquared = QuickMath::vectorLengthSquared(ball.pos - vecIntersect);

                if (!collisionHappened || distSquared < distSquaredNearest)
                {
                    vecIntersectNearest = vecIntersect;
                    distSquaredNearest = distSquared;
                    isHorizontal = (i % 2 == 0);
                    brickCollision = &brick;
                    collisionHappened = true;
                }
            }
        }
    }

    if (collisionHappened)
    {
        if (data.debug.drawPath)
        {
            data.debug.trails.emplace_back(sf::Vector2f(vecIntersectNearest.x - 10, vecIntersectNearest.y - 10), sf::Vector2f(vecIntersectNearest.x + 10, vecIntersectNearest.y + 10), sf::Color::Yellow);
            data.debug.trails.emplace_back(sf::Vector2f(vecIntersectNearest.x- 10, vecIntersectNearest.y + 10), sf::Vector2f(vecIntersectNearest.x + 10, vecIntersectNearest.y - 10), sf::Color::Yellow);
        }

        if (isHorizontal)
            ball.dir.y *= -1;
        else
            ball.dir.x *= -1;

        //ball.dir = QuickMath::rotateRandom(ball.dir, -5, 5);

        // set ball position to where the collision is and calculate rest of the travelling distance recursively
        sf::Vector2f posOld = ball.pos;
        //ball.pos = ball.pos + ball.dir * distToWall;
        ball.pos = vecIntersectNearest;

        if (data.debug.drawPath)
        {
            DebugData::Line line(posOld, ball.pos, sf::Color::Red);
            data.debug.trails.emplace_back(line);
        }

        handleCollision(ball, *brickCollision);

        calcTargetPosWithCollisions(ball, dist - std::sqrt(distSquaredNearest));
    }
    else
    {
        // no collision at all
        sf::Vector2f posOld = ball.pos;
        ball.pos = targetPos;
        if (data.debug.drawPath)
        {
            DebugData::Line line(posOld, ball.pos, sf::Color::White);
            data.debug.trails.emplace_back(line);
        }
    }
}

void MovementSystem::handleCollision(Ball &ball, Brick &brick)
{
    // we collide
    int damage = data.calculations.ballDamage[ball.type];
    int damageDone = brick.attack(damage);
    data.player.money += damageDone;

    data.ui.menuHud.labelDebug->setText(fmt::format("dam: {}", damageDone));


    switch (ball.type)
    {
    case Ball::Type::eSplatter:
    {
        if (!brick.isDestructible())
            break;

        for (auto &b : data.entity.bricks)
        {
            if (&b != &brick)
            {
                //if (QM::vectorLength((ball.pos - (b.pos + b.size / 2))) < data.statistics.ballRange[Ball::Type::eSplatter])
                if (QuickMath::getDistance(ball.pos, b.pos + b.size / 2) < data.calculations.ballRange[Ball::Type::eSplatter])
                {
                    int damageDone = b.attack(static_cast<int>(std::round(damage / 2.0)));
                    data.player.money += damageDone;
                }
            }
        }
    }
        break;
    case Ball::Type::eSniper:
    {
        if (!brick.isDestructible())
        {
            Brick *brickClosest = nullptr;
            for (auto &b : data.entity.bricks)
            {
                if (b.isEnabled() && b.isDestructible())
                {
                    if (brickClosest == nullptr || QuickMath::vectorLengthSquared(ball.pos - (b.pos + b.size / 2)) < QuickMath::vectorLengthSquared(ball.pos - (brickClosest->pos + brickClosest->size / 2)))
                    {
                        brickClosest = &b;
                    }
                }
            }

            if (brickClosest == nullptr)
                break;

            ball.dir = -QuickMath::getUnitVector(ball.pos - brickClosest->pos - brickClosest->size / 2);

            //data.ui.menuHud.labelDebug->setText(fmt::format("{}: {},{} - speed: {}", debCount++, ball.dir.x, ball.dir.y, std::abs(ball.dir.x) + std::abs(ball.dir.y)));
        }
    }
        break;
    case Ball::Type::eBall:
    case Ball::Type::eInvalid:
        break;
    }

}

/*if (!brick.destructible())
{
    auto itBrickClosest = std::min_element(data.entity.bricks.begin(), data.entity.bricks.end(), [&ball]( const Brick &a, const Brick &b )
    {
        if (!b.destructible() || !a.destructible())
            return false;

        return QM::vectorLengthSquared(ball.pos - a.pos) < QM::vectorLengthSquared(ball.pos - b.pos);
    });

    auto angle = QM::angleOfLine(ball.pos, itBrickClosest->pos);

    //data.debug.trails.push_back(DebugData::Line(ball.pos, itBrickClosest->pos, sf::Color::Cyan));

    ball.dir = { 1, 0 };
    ball.dir = QM::rotate(ball.dir, angle);

    data.ui.menuHud.labelDebug->setText(fmt::format("{}: {}", debCount++, angle));
}*/
