#include "statisticssystem.h"

#include "../entities/ball.h"

#include "../data/debugdata.h"
#include "../data/entitydata.h"
#include "../data/outputdata.h"
#include "../data/eventdata.h"
#include "../data/calculationdata.h"
#include "../data/playerdata.h"

#include <cmath>


using namespace std::chrono_literals;

StatisticsSystem::StatisticsSystem(DataRef &dataRef)
    : System(dataRef)
{}

void StatisticsSystem::preUpdate()
{
    // precalculate statistics
    for (const Ball::Type ballType : listEnumBallType)
    {
        data.calculations.ballSpeed[ballType] = ConstBall::mapBallDataConst.at(ballType).baseSpeed * data.player.mapBallDataMod.at(ballType).upgrades[Ball::Upgrade::eSpeed];
        data.calculations.ballDamage[ballType] = ConstBall::mapBallDataConst.at(ballType).baseDamage * data.player.mapBallDataMod.at(ballType).upgrades[Ball::Upgrade::eDamage];
        data.calculations.ballRange[ballType] = ConstBall::mapBallDataConst.at(ballType).baseRange * data.player.mapBallDataMod.at(ballType).upgrades[Ball::Upgrade::eRange];

        int basePrice = ConstBall::mapBallDataConst.at(ballType).basePrice;
        int price = basePrice + static_cast<int>(std::pow(basePrice, data.player.mapBallDataMod.at(ballType).count / 3.0));
        data.calculations.ballBuyPrice[ballType] = price;

        // very tmp
        data.calculations.ballSellPrice[ballType] = std::lround(std::sqrt(price));


        for (const auto upgrade : ConstBall::mapBallDataConst.at(ballType).validUpgrades)
        {
            data.calculations.ballPriceUpgrade[ballType][upgrade] = static_cast<money_t>(std::pow(10, data.player.mapBallDataMod.at(ballType).upgrades[upgrade] + 1) / 2);
        }
    }
}

void StatisticsSystem::update(std::chrono::microseconds /*timeDiff*/)
{

}

void StatisticsSystem::postUpdate()
{

}

void StatisticsSystem::processActionEvents()
{
    /*for (const auto &action : data.event.actionEvents)
    {
        switch (action)
        {
        default:
            break;
        }
    }*/
}

