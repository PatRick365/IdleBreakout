#pragma once

#include "system.h"

#include <unordered_set>


class DebugSystem : public System
{
public:
    explicit DebugSystem(DataRef &dataRef);

    void preUpdate() override;
    void update(std::chrono::microseconds timeDiff) override;
    void postUpdate() override;

    void processActionEvents() override;

};
