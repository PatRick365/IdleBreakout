#include "debugsystem.h"

#include "../utility/quickmath.h"

#include "../data/debugdata.h"
#include "../data/entitydata.h"
#include "../data/outputdata.h"
#include "../data/eventdata.h"
#include "../data/playerdata.h"
#include "../data/uidata.h"

#include <fmt/format.h>


using namespace std::chrono_literals;

DebugSystem::DebugSystem(DataRef &dataRef)
    : System(dataRef)
{}

void DebugSystem::preUpdate()
{

}

void DebugSystem::update(std::chrono::microseconds timeDiff)
{
//    if (data.entity.balls.size() > 0)
//        data.ui.menuHud.labelDebug->setText(fmt::format("{},{} - speed: {}", data.entity.balls.front().dir.x, data.entity.balls.front().dir.y, QM::vectorLength(data.entity.balls.front().dir)));

    ++data.debug.frameCount;
    data.debug.frameCountdown += timeDiff;
    if (data.debug.frameCountdown >= 500ms)
    {
        data.debug.frameLastFps = data.debug.frameCount * 2;
        data.debug.frameCount = 0;
        data.debug.frameCountdown = 0us;
    }

    if (data.event.releasedKeys.count(sf::Keyboard::F3))
        data.debug.showFps = !data.debug.showFps;

    if (data.event.pressedKeys.count(sf::Keyboard::Add))
        data.player.money += 1000;

    if (data.event.releasedKeys.count(sf::Keyboard::Multiply))
        for (auto &brick : data.entity.bricks)
            brick.attack(100000000);

    while(data.debug.trails.size() > 1000)
        data.debug.trails.pop_front();

}

void DebugSystem::postUpdate()
{

}

void DebugSystem::processActionEvents()
{
    /*for (const auto &action : data.event.actionEvents)
    {
        switch (action)
        {
        default:
            break;
        }
    }*/
}
