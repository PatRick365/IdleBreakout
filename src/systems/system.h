#pragma once

#include "../data/dataref.h"


#include <chrono>


//namespace sf { class RenderWindow; }
//class UiManager;

class System
{
public:
    explicit System(DataRef &dataRef);
    virtual ~System() = default;

    virtual void preUpdate() = 0;
    virtual void update(std::chrono::microseconds timeDiff) = 0;
    virtual void postUpdate() = 0;


    virtual void processActionEvents() = 0;


protected:
    DataRef &data;

    //sf::RenderWindow &renderWindow;
    //UiManager &uiManager;


};
