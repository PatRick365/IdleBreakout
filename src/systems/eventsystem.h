#pragma once

#include "system.h"

#include <unordered_set>


class EventSystem : public System
{
public:
    explicit EventSystem(DataRef &dataRef);

    void preUpdate() override;
    void update(std::chrono::microseconds timeDiff) override;
    void postUpdate() override;

    void processActionEvents() override;


};
