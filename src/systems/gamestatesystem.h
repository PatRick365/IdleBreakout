#pragma once

#include "system.h"


class GameStateSystem : public System
{
public:
    explicit GameStateSystem(DataRef &dataRef);

    void preUpdate() override;
    void update(std::chrono::microseconds timeDiff) override;
    void postUpdate() override;

    void processActionEvents() override;


};

