#pragma once

#include "system.h"

#include <chrono>


class SaveStateSystem : public System
{
public:
    explicit SaveStateSystem(DataRef &dataRef);

    void preUpdate() override;
    void update(std::chrono::microseconds timeDiff) override;
    void postUpdate() override;

    void processActionEvents() override;


private:
    std::chrono::microseconds m_saveTimerTime;
    std::chrono::microseconds m_saveTimer;

};

