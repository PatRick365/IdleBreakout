#include "rendersystem.h"

#include "../screenlayout.h"

#include "../data/debugdata.h"
#include "../data/entitydata.h"
#include "../data/outputdata.h"
#include "../data/uidata.h"

#include "../utility/colors.h"

#include <TGUI/TGUI.hpp>

#include <fmt/format.h>

#include <array>


RenderSystem::RenderSystem(DataRef &dataRef)
    : System(dataRef)
{
    m_fontMexicanTequila.loadFromFile("data/resources/fonts/MexicanTequila.ttf");
    m_fontCapture_it.loadFromFile("data/resources/fonts/Capture_it.ttf");
    m_fontCollegiate.loadFromFile("data/resources/fonts/CollegiateBlackFLF.ttf");

    m_textureBall.loadFromFile("data/resources/sprites/ball.png");
    m_textureSplatter.loadFromFile("data/resources/sprites/splatter.png");
    m_textureSniper.loadFromFile("data/resources/sprites/sniper.png");

}

void RenderSystem::preUpdate()
{}

void RenderSystem::update(std::chrono::microseconds /*timeDiff*/)
{}

void RenderSystem::postUpdate()
{
    sf::View view(sf::FloatRect({0, 0}, sf::Vector2f(ConstScreenLayout::screenWidth, ConstScreenLayout::screenHeight)));
    data.output.renderWindow->setView(view);
    data.output.renderWindow->clear(sf::Color::Black);

/// BALLS
    for (const Ball &ball : data.entity.balls)
    {
        sf::Sprite sprite;
        switch (ball.type)
        {
        case Ball::Type::eBall:
            sprite = sf::Sprite(m_textureBall);
            break;
        case Ball::Type::eSplatter:
            sprite = sf::Sprite(m_textureSplatter);
            break;
        case Ball::Type::eSniper:
            sprite = sf::Sprite(m_textureSniper);
            break;
        case Ball::Type::eInvalid:
            break;
        }

        float ballRadius = ConstBall::mapBallDataConst.at(ball.type).radius;

        sprite.setPosition(sf::Vector2f(ball.pos.x - ballRadius, ball.pos.y - ballRadius));
        data.output.renderWindow->draw(sprite);
    }

/// BRICKS
    sf::RectangleShape shapeRect;
    int widthOutline = 3;
    shapeRect.setOutlineThickness(widthOutline);

    sf::Text textHealth("", m_fontCollegiate, 20);

    for (const Brick &brick : data.entity.bricks)
    {
        if (!brick.isEnabled())
            continue;

        shapeRect.setPosition(brick.pos + sf::Vector2f(widthOutline, widthOutline));
        shapeRect.setSize(brick.size - sf::Vector2f(widthOutline * 2, widthOutline * 2));
        shapeRect.setFillColor(brick.color());
        shapeRect.setOutlineColor(brick.colorOutline());
        data.output.renderWindow->draw(shapeRect);

        // draw health
        //if (brick.type == Brick::Type::eBrick || brick.type == Brick::Type::eBoss)
        if (brick.isDestructible())
        {
            int sum = brick.color().r + brick.color().g + brick.color().b;
            sf::Color colorText = (sum > 255 * 3 / 2) ? sf::Color::Black : sf::Color::White;
            textHealth.setFillColor(colorText);

            textHealth.setString(std::to_string(brick.health()));
            textHealth.setPosition(brick.pos + sf::Vector2f(8, 8));
            data.output.renderWindow->draw(textHealth);
        }
    }

/// DEBUG PATH
    for (const DebugData::Line &line : data.debug.trails)
    {
        sf::Vertex verticles[] = { sf::Vertex(line.points[0], line.col), sf::Vertex(line.points[1], line.col) };
        data.output.renderWindow->draw(verticles, 2, sf::Lines);
    }

/// UI
    data.ui.tgui->draw();

/// DEBUG OVERLAY
    if (data.debug.showFps)
    {
        sf::Text frameRateText(std::to_string(data.debug.frameLastFps), m_fontCollegiate, 24);
        frameRateText.setPosition(1350, 0);
        frameRateText.setFillColor(sf::Color::Red);
        data.output.renderWindow->draw(frameRateText);

        sf::Text ballCountText(std::to_string(data.entity.balls.size()), m_fontCollegiate, 24);
        ballCountText.setPosition(1500, 0);
        ballCountText.setFillColor(sf::Color::Red);
        data.output.renderWindow->draw(ballCountText);
    }
    //auto v = data.ui.tgui->getView();
    //fmt::print("C:{}/{} - SIZE:{}/{}\n", v.getCenter().x, v.getCenter().y, v.getSize().x, v.getSize().y);

    data.output.renderWindow->display();
}

void RenderSystem::processActionEvents()
{
    /*for (const auto &action : data.event.actionEvents)
    {
        switch (action)
        {
        default:
            break;
        }
    }*/
}

