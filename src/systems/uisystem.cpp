#include "uisystem.h"

#include "../types.h"

#include "gamestatesystem.h"

#include "../data/gamestatedata.h"
#include "../data/outputdata.h"
#include "../data/calculationdata.h"
#include "../data/playerdata.h"
#include "../data/uidata.h"
#include "../data/settingsdata.h"

#include <TGUI/TGUI.hpp>

#include <fmt/format.h>


using namespace std::chrono_literals;

UiSystem::UiSystem(DataRef &dataRef)
    : System(dataRef)
{
/// GAME MENU
    for (const auto ballType : listEnumBallType)
    {
        for (const auto upgrade : ConstBall::mapBallDataConst.at(ballType).validUpgrades)
        {
            data.ui.menuGame.mapRowBallStore[ballType].mapButtonsUpgrades[upgrade]->connect("pressed", &UiSystem::buyUpgrade, this, ballType, upgrade);
        }
    }


/// SETTINGS MENU
    data.ui.menuSettings.sliderMaxFramerate->connect("ValueChanged", [this](){
        auto maxFps = static_cast<unsigned int>(data.ui.menuSettings.sliderMaxFramerate->getValue());
        data.settings.graphics.maxFps = maxFps;
        data.output.renderWindow->setFramerateLimit(maxFps);
    });

    data.ui.menuSettings.checkBoxVsync->connect("Changed", [this](){
        bool isVsyncEnabled = data.ui.menuSettings.checkBoxVsync->isChecked();
        data.settings.graphics.isVsyncEnabled = isVsyncEnabled;
        data.output.renderWindow->setVerticalSyncEnabled(isVsyncEnabled);
    });

    data.ui.menuSettings.checkBoxFullScreen->connect("Changed", [this](){
        bool isFullscreenEnabled = data.ui.menuSettings.checkBoxFullScreen->isChecked();
        bool fullscreenChanged = (isFullscreenEnabled != data.settings.graphics.isFullscreenEnabled);
        if (fullscreenChanged)
        {
            data.settings.graphics.isFullscreenEnabled = isFullscreenEnabled;
            data.output.renderWindow->create(sf::VideoMode(data.settings.graphics.resolutionX, data.settings.graphics.resolutionY),
                                            "Idle", isFullscreenEnabled ? sf::Style::Fullscreen : sf::Style::Default);
        }
    });


    // init ui from settings
    data.ui.menuSettings.checkBoxVsync->setChecked(data.settings.graphics.isVsyncEnabled);
    data.ui.menuSettings.checkBoxFullScreen->setChecked(data.settings.graphics.isFullscreenEnabled);
    data.ui.menuSettings.sliderMaxFramerate->setValue(data.settings.graphics.maxFps);

}

void UiSystem::preUpdate()
{

}

void UiSystem::update(std::chrono::microseconds /*timeDiff*/)
{
    data.ui.menuHud.labelMoney->setText(fmt::format("{}$", data.player.money));

    data.ui.menuHud.buttonBuyBall->setText(fmt::format("{}\n{}$", ConstBall::mapBallDataConst.at(Ball::Type::eBall).name, data.calculations.ballBuyPrice[Ball::Type::eBall]));
    data.ui.menuHud.buttonBuyBall->setEnabled(data.calculations.ballBuyPrice[Ball::Type::eBall] <= data.player.money);

    data.ui.menuHud.buttonBuySplatter->setText(fmt::format("{}\n{}$", ConstBall::mapBallDataConst.at(Ball::Type::eSplatter).name, data.calculations.ballBuyPrice[Ball::Type::eSplatter]));
    data.ui.menuHud.buttonBuySplatter->setEnabled(data.calculations.ballBuyPrice[Ball::Type::eSplatter] <= data.player.money);

    data.ui.menuHud.buttonBuySniper->setText(fmt::format("{}\n{}$", ConstBall::mapBallDataConst.at(Ball::Type::eSniper).name, data.calculations.ballBuyPrice[Ball::Type::eSniper]));
    data.ui.menuHud.buttonBuySniper->setEnabled(data.calculations.ballBuyPrice[Ball::Type::eSniper] <= data.player.money);

    if (data.ui.menuGame.window->isVisible())
    {
        for (const auto ballType : listEnumBallType)
        {
            //if (data.ui.menuGame.mapRowBallStore[ballType].needsUpdate())
            {
                data.ui.menuGame.mapRowBallStore[ballType].buttonBallBuy->setText(fmt::format("Buy\n{}$", data.calculations.ballBuyPrice[ballType]));
                data.ui.menuGame.mapRowBallStore[ballType].buttonBallBuy->setEnabled(data.player.money >= data.calculations.ballBuyPrice[ballType]);

                bool gotBall = (data.player.mapBallDataMod[ballType].count > 0);
                data.ui.menuGame.mapRowBallStore[ballType].buttonBallSell->setEnabled(gotBall);

                data.ui.menuGame.mapRowBallStore[ballType].setUnlocked(data.player.mapBallDataMod[ballType].unlocked);
                data.ui.menuGame.mapRowBallStore[ballType].setGotBall(gotBall);


                if (gotBall)
                    data.ui.menuGame.mapRowBallStore[ballType].buttonBallSell->setText(fmt::format("Sell\n{}$", data.calculations.ballSellPrice[ballType]));
                else
                    data.ui.menuGame.mapRowBallStore[ballType].buttonBallSell->setText("Sell");


                for (const auto upgrade : ConstBall::mapBallDataConst.at(ballType).validUpgrades)
                {
                    data.ui.menuGame.mapRowBallStore[ballType].mapButtonsUpgrades[upgrade]->setText(fmt::format("Upgrade\n{}\n{}$",toString(upgrade),
                                                                                                                data.calculations.ballPriceUpgrade[ballType][upgrade]));
                    data.ui.menuGame.mapRowBallStore[ballType].mapButtonsUpgrades[upgrade]->setEnabled(data.player.money >= data.calculations.ballPriceUpgrade[ballType][upgrade]);
                }
            }
        }
    }

    if (data.ui.menuSettings.window->isVisible())
    {
        data.ui.menuSettings.labelMaxFramerate->setText(std::to_string(data.ui.menuSettings.sliderMaxFramerate->getValue()));
    }


}

void UiSystem::postUpdate()
{

}

void UiSystem::processActionEvents()
{
    /*for (const auto &action : data.event.actionEvents)
    {
        switch (action)
        {
        default:
            break;
        }
    }*/
}

void UiSystem::buyUpgrade(Ball::Type ballType, Ball::Upgrade upgrade)
{
    if (data.player.mapBallDataMod[ballType].count <= 0) // should not happen because ui is locked
        return;

    money_t price = data.calculations.ballPriceUpgrade[ballType][upgrade];
    if (data.player.money >= price)
    {
        data.player.money -= price;
        ++data.player.mapBallDataMod.at(ballType).upgrades[upgrade];
        //data.ui.menuGame.mapRowBallStore[ballType].flagForUpdate();
    }
}
