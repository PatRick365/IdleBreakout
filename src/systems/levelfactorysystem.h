#pragma once

#include "system.h"

#include <vector>
#include <string>

class LevelFactorySystem : public System
{
public:
    explicit LevelFactorySystem(DataRef &dataRef);

    void preUpdate() override;
    void update(std::chrono::microseconds timeDiff) override;
    void postUpdate() override;

    void processActionEvents() override;

private:
    std::vector<std::vector<std::string>> m_levelData;

};
