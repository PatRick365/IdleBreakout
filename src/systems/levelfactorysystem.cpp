#include "levelfactorysystem.h"

#include "../screenlayout.h"

#include "../utility/quickrand.h"
#include "../utility/quickmath.h"

#include "../data/entitydata.h"
#include "../data/gamestatedata.h"
#include "../data/playerdata.h"

#include <fmt/format.h>

#include <fstream>
#include <filesystem>


namespace fs = std::filesystem;

static const std::string dirLevels("data/levels");
static const std::string extensionLevels(".csv");

std::vector<std::string> loadLevelFromFile(const std::string &fileLevel)
{
    fmt::print("loading " + fileLevel + "...\n");

    std::vector<std::string> data;

    std::ifstream file(fileLevel);
    std::string delimiter = ";";
    size_t pos = 0;
    std::string line;

    while (getline(file, line))
    {
        std::string token;

        while ((pos = line.find(delimiter)) != std::string::npos)
        {
            token = line.substr(0, pos);
            data.push_back(token);
            line.erase(0, pos + delimiter.length());
        }
        data.push_back(line);
    }
    file.close();

    return data;
}

LevelFactorySystem::LevelFactorySystem(DataRef &dataRef)
    : System(dataRef)
{
    auto levelCount = std::distance(fs::directory_iterator(dirLevels), fs::directory_iterator{});
    m_levelData.reserve(static_cast<size_t>(levelCount));

    size_t shouldBeSize = ConstBrick::levelBrickWidth * ConstBrick::levelBrickHeight;

    for (const auto &entry : fs::directory_iterator(dirLevels))
    {
        if (entry.path().extension() != extensionLevels)
            continue;

        m_levelData.emplace_back(loadLevelFromFile(entry.path()));

        if (m_levelData.back().size() != shouldBeSize)
        {
            fmt::print("Invalid level size: ({} != {}). Exiting...\n", m_levelData.back().size(), shouldBeSize);
            exit(1);
        }
    }
}

void LevelFactorySystem::preUpdate()
{

}

void LevelFactorySystem::update(std::chrono::microseconds /*timeDiff*/)
{
    if (data.gameState.state != GameStateData::State::LevelLoad)
        return;


    unsigned int wallHeight = 20;
    unsigned int wallWidth = 20;

    data.entity.bricks.clear();
    data.entity.spawnPoints.clear();

    /// @todo make sensible

    // upper
    data.entity.bricks.emplace_back(Brick::Type::eWall, sf::Vector2f(ConstScreenLayout::playBorderX, ConstScreenLayout::playBorderY - wallHeight),
                                    sf::Vector2f(ConstScreenLayout::playBorderWidth, wallHeight));
    // lower
    data.entity.bricks.emplace_back(Brick::Type::eWall, sf::Vector2f(ConstScreenLayout::playBorderX, ConstScreenLayout::screenHeight - wallHeight),
                                    sf::Vector2f(ConstScreenLayout::playBorderWidth, wallHeight));
    // left
    data.entity.bricks.emplace_back(Brick::Type::eWall, sf::Vector2f(ConstScreenLayout::playBorderX - wallWidth, ConstScreenLayout::playBorderY - wallHeight),
                                    sf::Vector2f(wallWidth, ConstScreenLayout::playBorderHeight + wallHeight));
    // right
    data.entity.bricks.emplace_back(Brick::Type::eWall, sf::Vector2f(ConstScreenLayout::playBorderX + ConstScreenLayout::playBorderWidth, ConstScreenLayout::playBorderY - wallHeight),
                                    sf::Vector2f(wallWidth, ConstScreenLayout::playBorderHeight + wallHeight));


    if (data.player.currentLevel % 10)
    {
        size_t levelNumber = static_cast<size_t>(data.player.currentLevel) % m_levelData.size();
        size_t cellCount = m_levelData.at(levelNumber).size();
        for (size_t iCell = 0; iCell < cellCount; ++iCell)
        {
            size_t x = iCell % ConstBrick::levelBrickWidth;
            size_t y = iCell / ConstBrick::levelBrickWidth;

            //fmt::print("{},{};\n", ConstBrick::levelBrickWidth, ConstBrick::levelBrickHeight);

            // brick
            if (m_levelData.at(levelNumber).at(iCell) == "B")
            {
                data.entity.bricks.emplace_back(Brick::Type::eBrick, sf::Vector2f(x * ConstBrick::width + ConstScreenLayout::playBorderX,
                                                                                  y * ConstBrick::height + ConstScreenLayout::playBorderY),
                                                sf::Vector2f(ConstBrick::width,
                                                             ConstBrick::height),
                                                data.player.currentLevel);
            }
            // spawn point
            else if (m_levelData.at(levelNumber).at(iCell) == "S")
            {
                data.entity.spawnPoints.emplace_back(x * ConstBrick::width + ConstScreenLayout::playBorderX + ConstBrick::width / 2.0F,
                                                     y * ConstBrick::height + ConstScreenLayout::playBorderY + ConstBrick::height / 2.0F);
            }
        }

    }
    else
    {
        unsigned int bossWidth = ConstBrick::width * 5;
        unsigned int bossHeight = ConstBrick::height * 5;

        data.entity.bricks.emplace_back(Brick::Type::eBoss, sf::Vector2f(ConstScreenLayout::playBorderX + ConstScreenLayout::playBorderWidth / 2 - bossWidth / 2,
                                                                         ConstScreenLayout::playBorderY + ConstScreenLayout::playBorderHeight / 2 - bossHeight),
                                        sf::Vector2f(bossWidth, bossHeight),
                                        data.player.currentLevel * 250);


        data.entity.spawnPoints.emplace_back(ConstScreenLayout::playBorderX + ConstScreenLayout::playBorderWidth / 2,
                                             ConstScreenLayout::playBorderY + ConstScreenLayout::playBorderHeight - 200);
    }

    for (Ball &ball : data.entity.balls)
    {
        ball.pos = data.entity.spawnPoints.at(QuickRand::instance().get<size_t>(0, data.entity.spawnPoints.size() - 1));
        ball.dir = QuickMath::rotateRandom(sf::Vector2f(1, 0), 0, 360);
    }

    data.gameState.state = GameStateData::State::Running;
}

void LevelFactorySystem::postUpdate()
{

}

void LevelFactorySystem::processActionEvents()
{
    /*for (const auto &action : data.event.actionEvents)
    {
        switch (action)
        {
        default:
            break;
        }
    }*/
}
