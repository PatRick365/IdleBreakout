#include "spawningsystem.h"

#include "../utility/quickmath.h"
#include "../utility/quickrand.h"

#include "../data/debugdata.h"
#include "../data/entitydata.h"
#include "../data/outputdata.h"
#include "../data/uidata.h"
#include "../data/eventdata.h"
#include "../data/playerdata.h"
#include "../data/calculationdata.h"

#include <TGUI/TGUI.hpp>

#include <fmt/format.h>

#include <limits>


using namespace std::chrono_literals;

SpawningSystem::SpawningSystem(DataRef &dataRef)
    : System(dataRef)
{
/// HUD
    data.ui.menuHud.buttonBuyBall->connect("pressed", &SpawningSystem::buyBall, this, Ball::Type::eBall);
    data.ui.menuHud.buttonBuySplatter->connect("pressed", &SpawningSystem::buyBall, this, Ball::Type::eSplatter);
    data.ui.menuHud.buttonBuySniper->connect("pressed", &SpawningSystem::buyBall, this, Ball::Type::eSniper);


/// GAME MENU
    for (const auto ballType : listEnumBallType)
    {
        data.ui.menuGame.mapRowBallStore[ballType].buttonBallBuy->connect("pressed", &SpawningSystem::buyBall, this, ballType);
        data.ui.menuGame.mapRowBallStore[ballType].buttonBallSell->connect("pressed", &SpawningSystem::sellBall, this, ballType);
    }

/// DEBUG MENU
    data.ui.menuSettings.buttonClearBalls->connect("pressed", [this](){
        data.entity.balls.clear();

        for (const auto ballType : listEnumBallType)
        {
            data.player.mapBallDataMod[ballType].count = 0;
        }
    });

    data.ui.menuSettings.buttonNoMoney->connect("pressed", [this](){
        data.player.money = 0;
    });
    data.ui.menuSettings.buttonGetMoney->connect("pressed", [this](){
        data.player.money = std::numeric_limits<decltype(data.player.money)>().max() / 3;
    });

    data.ui.menuSettings.buttonUnlockAll->connect("pressed", [this](){
        for (const auto ballType : listEnumBallType)
        {
            data.player.mapBallDataMod[ballType].unlocked = true;
        }
    });



}


void SpawningSystem::preUpdate()
{

}

void SpawningSystem::update(std::chrono::microseconds /*timeDiff*/)
{
    if (data.event.pressedKeys.count(sf::Keyboard::C))
    {
        data.entity.balls.clear();
        data.debug.trails.clear();
    }

    if (data.event.pressedKeys.count(sf::Keyboard::RControl))
    {
        sf::Vector2i pixelPos = sf::Mouse::getPosition(*data.output.renderWindow);
        sf::Vector2f worldPos = data.output.renderWindow->mapPixelToCoords(pixelPos);

        data.entity.balls.emplace_back(Ball::Type::eBall, worldPos,
                                       randomizeVectorAngle(sf::Vector2f(1,0)),
                                       ConstBall::mapBallDataConst.at(Ball::Type::eBall).baseSpeed
                                       * data.player.mapBallDataMod[Ball::Type::eBall].upgrades[Ball::Upgrade::eSpeed]);
    }

    if (data.event.releasedKeys.count(sf::Keyboard::A))
    {
        spawnBall(Ball::Type::eBall);
    }
    if (data.event.pressedKeys.count(sf::Keyboard::B))
    {
        spawnBall(Ball::Type::eBall);
    }
}

void SpawningSystem::postUpdate()
{

}

sf::Vector2f SpawningSystem::randomizeVectorAngle(const sf::Vector2f &vec)
{
        return QuickMath::rotateRandom(vec, 0, 360);
}

sf::Vector2f SpawningSystem::getRandomSpanwPoint()
{
    std::uniform_int_distribution<> dist(0, static_cast<int>(data.entity.spawnPoints.size() - 1));
    size_t spawnIndex = QuickRand::instance().get<size_t>(0, data.entity.spawnPoints.size() - 1);
    return data.entity.spawnPoints[spawnIndex];

    //return sf::Vector2f(QuickRand::instance().get<float>(1920 / 2 - 100, 1920 / 2 + 100), QuickRand::instance().get<float>(1080 / 2 - 100, 1080 / 2 + 100));
}

void SpawningSystem::spawnBall(Ball::Type type)
{
    data.entity.balls.emplace_back(type, getRandomSpanwPoint(),
                                    randomizeVectorAngle(sf::Vector2f(1, 0)),
                                    ConstBall::mapBallDataConst.at(type).baseSpeed);
}

void SpawningSystem::buyBall(Ball::Type ballType)
{
    if (!data.player.mapBallDataMod[ballType].unlocked) // should not happen
        return;

    money_t price = data.calculations.ballBuyPrice[ballType];
    if (data.player.money >= price)
    {
        data.player.money -= price;
        ++data.player.mapBallDataMod.at(ballType).count;
        spawnBall(ballType);
        //data.ui.menuGame.mapRowBallStore[ballType].flagForUpdate();

        Ball::Type higherBall = Ball::getHigherBall(ballType);

        if (higherBall != Ball::Type::eInvalid)
            data.player.mapBallDataMod[higherBall].unlocked = true;
    }
}

void SpawningSystem::sellBall(Ball::Type ballType)
{
    if (data.player.mapBallDataMod[ballType].count > 0)
    {
        data.player.money += data.calculations.ballSellPrice[ballType];

        for (auto it = data.entity.balls.begin(); it != data.entity.balls.end(); ++it)
        {
            if (it->type == ballType)
            {
                data.entity.balls.erase(it);
                --data.player.mapBallDataMod.at(ballType).count;
                //data.ui.menuGame.mapRowBallStore[ballType].flagForUpdate();
                return;
            }
        }
    }
}

//BuyBallSniper

void SpawningSystem::processActionEvents()
{
    /*for (const auto &action : data.event.actionEvents)
    {
        switch (action)
        {
        case ActionId::eBuyBallBall:
        {
            buyBall(Ball::Type::eBall);
        }
            break;
        case ActionId::eBuyBallSplatter:
        {
            buyBall(Ball::Type::eSplatter);
        }
            break;
        case ActionId::eBuyBallSniper:
        {
            buyBall(Ball::Type::eSniper);
        }
            break;
        default:
            break;
        }
    }*/
}

