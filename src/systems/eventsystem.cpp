#include "eventsystem.h"

#include "../data/gamestatedata.h"
#include "../data/entitydata.h"
#include "../data/outputdata.h"
#include "../data/uidata.h"
#include "../data/eventdata.h"
#include "../data/playerdata.h"
#include "../data/debugdata.h"
#include "../data/calculationdata.h"

#include "../entities/ball.h"

#include <TGUI/TGUI.hpp>

#include <fmt/format.h>


EventSystem::EventSystem(DataRef &dataRef)
    : System(dataRef)
{}

void EventSystem::preUpdate()
{
    data.event.releasedKeys.clear();
    data.event.releasedMouseButtons.clear();
    data.event.mouseMoved = false;
}

void EventSystem::update(std::chrono::microseconds /*timeDiff*/)
{
    sf::Event event{};

    while (data.output.renderWindow->pollEvent(event))
    {
        /// @todo maybe continue only on mouse events
        if (data.ui.tgui->handleEvent(event))
            continue;

        if (event.type == sf::Event::Closed)
        {
            data.output.renderWindow->close();
        }

        if (event.type == sf::Event::KeyPressed)
        {
            data.event.pressedKeys.insert(event.key.code);
        }
        else if (event.type == sf::Event::KeyReleased)
        {
            data.event.releasedKeys.insert(event.key.code);
            data.event.pressedKeys.erase(event.key.code);
        }

        if (event.type == sf::Event::MouseButtonPressed)
        {
            data.event.pressedMouseButtons.insert(event.mouseButton.button);
        }
        else if (event.type == sf::Event::MouseButtonReleased)
        {
            data.event.releasedMouseButtons.insert(event.mouseButton.button);
            data.event.pressedMouseButtons.erase(event.mouseButton.button);
        }
        else if (event.type == sf::Event::MouseMoved)
        {
            data.event.mouseMoved = true;
        }
    }

    if (data.event.hasMouseEvent())
    {
        data.event.mousePos = data.output.renderWindow->mapPixelToCoords(sf::Mouse::getPosition(*data.output.renderWindow));
    }

    if (data.event.releasedMouseButtons.count(sf::Mouse::Left))
    {
        for (auto &brick : data.entity.bricks)
        {
            if ((data.event.mousePos.x >= brick.pos.x) && (data.event.mousePos.y >= brick.pos.y)
                    && (data.event.mousePos.x <= brick.pos.x + brick.size.x) && (data.event.mousePos.y <= brick.pos.y + brick.size.y))
            {
                if (brick.isEnabled())
                {
                    int damage = 1;
                    int damageDone = brick.attack(damage /*data.statistics.ballDamage[ball.type]*/); // need another statistic for this
                    data.player.money += damageDone;
                    break;
                }
            }
        }
    }

    // TEMP
    if (data.event.releasedKeys.count(sf::Keyboard::Escape))
    {
        data.output.renderWindow->close();
    }





/*
    if (data.event.releasedKeys.count(sf::Keyboard::Num1))
        data.player.ballSpeedUpgrade[Ball::Type::eBall] = 1;
    if (data.event.releasedKeys.count(sf::Keyboard::Num2))
        data.player.ballSpeedUpgrade[Ball::Type::eBall] = 2;
    if (data.event.releasedKeys.count(sf::Keyboard::Num3))
        data.player.ballSpeedUpgrade[Ball::Type::eBall] = 3;
    if (data.event.releasedKeys.count(sf::Keyboard::Num4))
        data.player.ballSpeedUpgrade[Ball::Type::eBall] = 4;
    if (data.event.releasedKeys.count(sf::Keyboard::Num5))
        data.player.ballSpeedUpgrade[Ball::Type::eBall] = 5;
    if (data.event.releasedKeys.count(sf::Keyboard::Num6))
        data.player.ballSpeedUpgrade[Ball::Type::eBall] = 6;
    if (data.event.releasedKeys.count(sf::Keyboard::Num7))
        data.player.ballSpeedUpgrade[Ball::Type::eBall] = 7;
    if (data.event.releasedKeys.count(sf::Keyboard::Num8))
        data.player.ballSpeedUpgrade[Ball::Type::eBall] = 8;
    if (data.event.releasedKeys.count(sf::Keyboard::Num9))
        data.player.ballSpeedUpgrade[Ball::Type::eBall] = 9;
*/
    if (data.event.releasedKeys.count(sf::Keyboard::P))
    {
        data.debug.drawPath = !data.debug.drawPath;
        if (!data.debug.drawPath)
            data.debug.trails.clear();
    }



}

void EventSystem::postUpdate()
{

}

void EventSystem::processActionEvents()
{
    /*for (const auto &action : data.event.actionEvents)
    {
        switch (action)
        {
        case ActionId::eQuitGame:
        {
            data.output.renderWindow->close();
        }
            break;
        default:
            break;
        }
    }*/

}
