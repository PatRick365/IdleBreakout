#pragma once

#include "system.h"


class StatisticsSystem : public System
{
public:
    explicit StatisticsSystem(DataRef &dataRef);

    void preUpdate() override;
    void update(std::chrono::microseconds timeDiff) override;
    void postUpdate() override;

    void processActionEvents() override;

};
