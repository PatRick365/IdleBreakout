#include "savestatesystem.h"

#include "../filepaths.h"

#include "../utility/dataserialization.hpp"


using namespace std::chrono_literals;

SaveStateSystem::SaveStateSystem(DataRef &dataRef)
    : System(dataRef),
      m_saveTimerTime(1s),
      m_saveTimer(m_saveTimerTime)
{}

void SaveStateSystem::preUpdate()
{}

void SaveStateSystem::update(std::chrono::microseconds timeDiff)
{
    m_saveTimer -= timeDiff;

    if (m_saveTimer < 0us)
    {
        std::filesystem::create_directory(FilePaths::dirSave);

        DataSerialization::saveData<SettingsData>(data.settings, FilePaths::dirSave);
        DataSerialization::saveData<PlayerData>(data.player, FilePaths::dirSave);
        DataSerialization::saveData<EntityData>(data.entity, FilePaths::dirSave);

        m_saveTimer  = m_saveTimerTime;
    }
}

void SaveStateSystem::postUpdate()
{}

void SaveStateSystem::processActionEvents()
{
    /*for (const auto &action : data.event.actionEvents)
    {
        switch (action)
        {
        default:
            break;
        }
    }*/
}
