#pragma once

#include "system.h"

#include <SFML/Graphics.hpp>

#include <unordered_set>

class Ball;
class Brick;

class MovementSystem : public System
{
public:
    explicit MovementSystem(DataRef &dataRef);

    void calcTargetPosWithCollisions(Ball &ball, float dist);

    void handleCollision(Ball &ball, Brick &brick);

    void preUpdate() override;
    void update(std::chrono::microseconds timeDiff) override;
    void postUpdate() override;

    void processActionEvents() override;

};
