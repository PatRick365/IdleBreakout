#include "gamestatesystem.h"

#include "../data/gamestatedata.h"
#include "../data/entitydata.h"
#include "../data/playerdata.h"

#include <algorithm>


using namespace std::chrono_literals;

GameStateSystem::GameStateSystem(DataRef &dataRef)
    : System(dataRef)
{}

void GameStateSystem::preUpdate()
{

}

void GameStateSystem::update(std::chrono::microseconds /*timeDiff*/)
{

    if (data.gameState.state == GameStateData::State::Running)
    {

        bool levelStillGoing = false;
        for (const auto &brick : data.entity.bricks)
        {
            if (brick.isEnabled() && brick.isDestructible())
            {
                levelStillGoing = true;
                break;
            }
        }

        if (!levelStillGoing)
        {
            data.gameState.state = GameStateData::State::LevelLoad;
            ++data.player.currentLevel;    // BAD

        }
    }


}

void GameStateSystem::postUpdate()
{

}


void GameStateSystem::processActionEvents()
{
    /*for (const auto &action : data.event.actionEvents)
    {
        switch (action)
        {
        default:
            break;
        }
    }*/
}
