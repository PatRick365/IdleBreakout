#pragma once

#include "system.h"

#include "../entities/ball.h"


class UiSystem : public System
{
public:
    explicit UiSystem(DataRef &dataRef);

    void preUpdate() override;
    void update(std::chrono::microseconds timeDiff) override;
    void postUpdate() override;

    void processActionEvents() override;

//    void buyUpgradeSpeed(Ball::Type ballType);
//    void buyUpgradeDamage(Ball::Type ballType);
    void buyUpgrade(Ball::Type ballType, Ball::Upgrade upgrade);

};
