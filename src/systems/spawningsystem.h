#pragma once

#include "system.h"

#include "../entities/ball.h"

#include <SFML/Graphics.hpp>

class SpawningSystem : public System
{
public:
    explicit SpawningSystem(DataRef &dataRef);

    void preUpdate() override;
    void update(std::chrono::microseconds timeDiff) override;
    void postUpdate() override;

    void processActionEvents() override;


private:
    sf::Vector2f randomizeVectorAngle(const sf::Vector2f &vec);

    sf::Vector2f getRandomSpanwPoint();

    void spawnBall(Ball::Type type);
    void buyBall(Ball::Type type);
    void sellBall(Ball::Type type);

    //Ball(Ball::Type type, const sf::Vector2f &pos, float radius, sf::Vector2f dir, float speed);


private:

};


