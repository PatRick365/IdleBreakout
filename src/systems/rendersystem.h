#pragma once

#include "system.h"

#include <SFML/Graphics.hpp>


class RenderSystem : public System
{
public:
    explicit RenderSystem(DataRef &dataRef);

    void preUpdate() override;
    void update(std::chrono::microseconds timeDiff) override;
    void postUpdate() override;

    void processActionEvents() override;


private:
    sf::Texture m_tileTexture;
    sf::Texture m_entityTexture;

    sf::Texture m_textureBall;
    sf::Texture m_textureSplatter;
    sf::Texture m_textureSniper;

    sf::Font m_fontMexicanTequila;
    sf::Font m_fontCapture_it;
    sf::Font m_fontCollegiate;

};

