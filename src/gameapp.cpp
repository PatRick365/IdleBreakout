#include "gameapp.h"

#include "systems/debugsystem.h"
#include "systems/gamestatesystem.h"
#include "systems/statisticssystem.h"
#include "systems/levelfactorysystem.h"
#include "systems/eventsystem.h"
#include "systems/movementsystem.h"
#include "systems/rendersystem.h"
#include "systems/spawningsystem.h"
#include "systems/uisystem.h"
#include "systems/savestatesystem.h"

#include <SFML/Graphics.hpp>
#include <fmt/format.h>


using namespace std::chrono;
using namespace std::chrono_literals;

using time_stamp = std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds>;

GameApp::GameApp()
    : m_data(m_dataFactory.makeData())
{
    addSystem<GameStateSystem>(*m_data);
    addSystem<UiSystem>(*m_data);
    addSystem<StatisticsSystem>(*m_data);
    addSystem<LevelFactorySystem>(*m_data);
    addSystem<EventSystem>(*m_data);
    addSystem<SpawningSystem>(*m_data);
    addSystem<MovementSystem>(*m_data);
    addSystem<DebugSystem>(*m_data);
    addSystem<RenderSystem>(*m_data);
    addSystem<SaveStateSystem>(*m_data);
}

int GameApp::run()
{
    time_stamp timestamp = time_point_cast<microseconds>(system_clock::now());

    while (m_data->output.renderWindow->isOpen())
    {
        std::chrono::microseconds timeDiff = time_point_cast<microseconds>(system_clock::now()) - timestamp;
        timestamp = time_point_cast<microseconds>(system_clock::now());


        for (auto &system : m_systems)
            system->preUpdate();

        for (auto &system : m_systems)
            system->update(timeDiff);

        for (auto &system : m_systems)
            system->postUpdate();

    }

    return 0;
}
