#pragma once

#include <SFML/System/Vector2.hpp>

sf::Vector2f operator /(const sf::Vector2f &v, float m);

sf::Vector2f operator *(const sf::Vector2f &v, float m);

sf::Vector2f operator -(const sf::Vector2f &v, float m);

sf::Vector2f operator +(const sf::Vector2f &v, float m);
