#include "colors.h"


namespace ConstColors
{

const std::array<sf::Color, colorCount> palette =
{
    sf::Color(46,   139,    87),
    sf::Color(178,  34,     34),
    sf::Color(0,    0,      128),
    sf::Color(255,  165,    0),
    sf::Color(255,  255,    0),
    sf::Color(128,  128,    0),
    sf::Color(0,    255,    0),
    sf::Color(65,   105,    225),
    sf::Color(0,    255,    255),
    sf::Color(0,    191,    255),
    sf::Color(255,  0,      0),
    sf::Color(0,    0,      255),
    sf::Color(216,  191,    216),
    sf::Color(255,  0,      255),
    sf::Color(238,  232,    170),
    sf::Color(255,  20,     147),
    sf::Color(255,  160,    122),
    sf::Color(238,  130,    238),
    sf::Color(152,  251,    152),
    sf::Color(47,   79,     79)
};

} // namespace ConstColors
