#pragma once

/// @todo cleanup
/// move to datamanager maybe
#include "../data/entitydata.h"
#include "../data/playerdata.h"
#include "../data/settingsdata.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <filesystem>
#include <fstream>
#include <type_traits>

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive &ar, sf::Vector2f &vec, const unsigned int /*version*/)
{
    ar & vec.x;
    ar & vec.y;
}

} // namespace serialization
} // namespace boost

/// @todo: coul be a namespace ??

class DataSerialization
{
public:
    DataSerialization() = default;

    template<typename T>
    static void saveData(const T &data, const std::string &dirSave)
    {
        std::ofstream ofs(dirSave + "/" + T::className);
        boost::archive::text_oarchive oa(ofs);
        oa << data;
    }

    template<typename T>
    static T loadData(const std::string &dirSave)
    {
        std::string filePathName = dirSave + "/" + T::className;

        if (!std::filesystem::exists(filePathName))
        {
            return T();
        }

        T data;
        std::ifstream ifs(filePathName);
        boost::archive::text_iarchive ia(ifs);
        ia >> data;

        return data;
    }
};
