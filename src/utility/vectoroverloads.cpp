#include "vectoroverloads.h"


sf::Vector2f operator /(const sf::Vector2f &v, float m)
{
    return sf::Vector2f(v.x / m, v.y / m);
}

sf::Vector2f operator *(const sf::Vector2f &v, float m)
{
    return sf::Vector2f(v.x * m, v.y * m);
}

sf::Vector2f operator -(const sf::Vector2f &v, float m)
{
    return sf::Vector2f(v.x - m, v.y - m);
}

sf::Vector2f operator +(const sf::Vector2f &v, float m)
{
    return sf::Vector2f(v.x + m, v.y + m);
}
