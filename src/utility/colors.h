#pragma once

#include <SFML/Graphics/Color.hpp>

#include <array>


namespace ConstColors
{

constexpr static unsigned int colorCount = 20;

extern const std::array<sf::Color, colorCount> palette;
//extern const std::array<sf::Color, colorCount> paletteInverted;

} // namespace ConstColors
