#pragma once

#include <random>

class QuickRand
{
private:
    QuickRand();


public:
    QuickRand(const QuickRand&) = delete;
    QuickRand &operator=(const QuickRand &) = delete;
    QuickRand(QuickRand &&) = delete;
    QuickRand &operator=(QuickRand &&) = delete;

    static QuickRand &instance();

    template<typename T>
    typename std::enable_if<std::is_integral<T>::value, T>::type get(T floor, T ceil)
    {
        std::uniform_int_distribution<T> dist(floor, ceil);
        return dist(m_randomEngine);
    }

    template<typename T>
    typename std::enable_if<std::is_floating_point<T>::value, T>::type get(T floor, T ceil)
    {
        std::uniform_real_distribution<T> dist(floor, ceil);
        return dist(m_randomEngine);
    }


private:
    std::random_device m_randomDevice;
    std::mt19937 m_randomEngine;

};
