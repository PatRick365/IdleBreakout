#pragma once

#include "datafactory.h"

#include "data/dataref.h"

#include "systems/system.h"

#include <TGUI/TGUI.hpp>

#include <vector>
#include <array>
#include <set>
#include <chrono>
#include <memory>


class GameApp
{
public:
    GameApp();

    int run();


private:
    template<typename T>
    void addSystem(DataRef &data)
    {
        m_systems.push_back(std::make_unique<T>(data));
    }


private:
    DataFactory m_dataFactory;

    std::unique_ptr<DataRef> m_data;

    std::vector<std::unique_ptr<System>> m_systems;

};

