#pragma once

#include "data/dataref.h"

#include "data/debugdata.h"
#include "data/settingsdata.h"
#include "data/gamestatedata.h"
#include "data/calculationdata.h"
#include "data/uidata.h"
#include "data/entitydata.h"
#include "data/outputdata.h"
#include "data/eventdata.h"
#include "data/playerdata.h"

#include <memory>


class DataFactory
{
public:
    DataFactory();


    std::unique_ptr<DataRef> makeData();


private:
    DebugData debugData;
    SettingsData settingsData;
    GameStateData gameStateData;
    CalculationData calculationData;
    UiData uiData;
    EntityData entityData;    // includes balls, tilegrid, user created like fireballs or something
    OutputData outputData;    // renderwindow, soundmanager
    EventData eventData;
    PlayerData playerData;    // money, upgrades


    //std::unique_ptr<DataRef> m_dataRef;

};
