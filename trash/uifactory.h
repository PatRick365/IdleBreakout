#pragma once

#include "../data/dataref.h"

namespace UiFactory
{

void loadUi(DataRef &data);

}
