#include "uifactory.h"

#include "../screenlayout.h"

#include "../data/outputdata.h"
#include "../data/uidata.h"

#include <fmt/printf.h>


/*void onTabSelected(tgui::Gui& tgui, std::string selectedTab)
{
    if (selectedTab == "Game")
    {
        tgui.get("Game")->setVisible(true);
        tgui.get("Audio")->setVisible(false);
        tgui.get("Video")->setVisible(false);
    }
    else if (selectedTab == "Audio")
    {
        tgui.get("Game")->setVisible(false);
        tgui.get("Audio")->setVisible(true);
        tgui.get("Video")->setVisible(false);
    }
    else if (selectedTab == "Video")
    {
        tgui.get("Game")->setVisible(false);
        tgui.get("Audio")->setVisible(false);
        tgui.get("Video")->setVisible(true);
    }
}*/

namespace UiFactory
{

void loadUi(DataRef &data)
{
/// SETTINGS
    // use of layouts would be better
    data.ui.menuSettings.window = tgui::ChildWindow::create("SettingsWindow");
    data.ui.menuSettings.window->setPosition(ConstScreenLayout::uiWidth / 8, ConstScreenLayout::uiHeight / 12);
    data.ui.menuSettings.window->setSize(ConstScreenLayout::uiWidth / 4, ConstScreenLayout::uiHeight / 3);
    data.ui.menuSettings.window->setTitle("Menu");

    tgui::Grid::Ptr gridSettings = tgui::Grid::create();
    gridSettings->setPosition(0, 0);
    gridSettings->setSize("100%", "90%");
    data.ui.menuSettings.window->add(gridSettings);

    data.ui.menuSettings.tabs = tgui::Tabs::create();
    data.ui.menuSettings.tabs->setTabHeight(30);
    data.ui.menuSettings.tabs->setPosition(0, 0);
    data.ui.menuSettings.tabs->add("Game");
    data.ui.menuSettings.tabs->add("Audio");
    data.ui.menuSettings.tabs->add("Video");
    //data.ui.menuSettings.window->add(data.ui.menuSettings.tabs);
    gridSettings->addWidget(data.ui.menuSettings.tabs, 0, 0);
    gridSettings->add(data.ui.menuSettings.tabs);

    tgui::Panel::Ptr panelGame = tgui::Panel::create();
 /////panelGame->setSize({ "100%","100%" });
    panelGame->setSize(200, 200);
    panelGame->setPosition(data.ui.menuSettings.tabs->getPosition().x, data.ui.menuSettings.tabs->getPosition().y + 30);
    //data.ui.menuSettings.window->add(panelGame, "Game");
    gridSettings->addWidget(panelGame, 1, 1);
    gridSettings->add(panelGame, "Game");

    tgui::Panel::Ptr panelAudio = tgui::Panel::copy(panelGame);
    //data.ui.menuSettings.window->add(panelAudio, "Audio");
    gridSettings->addWidget(panelAudio, 1, 1);
    gridSettings->add(panelAudio, "Audio");

    tgui::Panel::Ptr panelVideo = tgui::Panel::copy(panelGame);
    //data.ui.menuSettings.window->add(panelVideo, "Video");
    gridSettings->addWidget(panelVideo, 1, 1);
    gridSettings->add(panelVideo, "Video");

    tgui::Grid::Ptr gridControl = tgui::Grid::create();
    data.ui.menuSettings.buttonCancel = tgui::Button::create("Cancel");
    gridControl->add(data.ui.menuSettings.buttonCancel);
    gridControl->addWidget(data.ui.menuSettings.buttonCancel, 0, 0);
    data.ui.menuSettings.buttonApply = tgui::Button::create("Apply");
    gridControl->add(data.ui.menuSettings.buttonApply);
    gridControl->addWidget(data.ui.menuSettings.buttonApply, 0, 1);
    data.ui.menuSettings.buttonOk = tgui::Button::create("Ok");
    gridControl->add(data.ui.menuSettings.buttonOk);
    gridControl->addWidget(data.ui.menuSettings.buttonOk, 0, 2);
    //data.ui.menuSettings.window->add(gridControl);

    gridSettings->addWidget(gridControl, 1, 1);
    gridSettings->add(gridControl);




    data.ui.menuSettings.tabs->select("Game");

    //data.ui.menuSettings.tabs->connect("TabSelected", onTabSelected, std::ref(*data.ui.tgui));

    panelAudio->setVisible(false);
    panelVideo->setVisible(false);

    {
        auto l = tgui::Label::create();
        l->setSize(100, 50);
        l->setText("111");
        panelGame->add(l);
    }
    {
        auto l = tgui::Label::create();
        l->setSize(100, 50);
        l->setText("222");
        panelAudio->add(l);
    }
    {
        auto gridVideo = tgui::Grid::create();
        gridVideo->setSize("80%","70%");
        gridVideo->setPosition("10%", "10%");

        /// @todo: at some point these need to be loaded from settings, maybe a function in here, in here called once, then in uisystem on change
        auto labelCheckBoxFullScreen = tgui::Label::create("Full screen");
        gridVideo->addWidget(labelCheckBoxFullScreen, 0, 0);
        gridVideo->add(labelCheckBoxFullScreen);
        data.ui.menuSettings.checkBoxFullScreen = tgui::CheckBox::create();
        gridVideo->addWidget(data.ui.menuSettings.checkBoxFullScreen, 0, 1);
        gridVideo->add(data.ui.menuSettings.checkBoxFullScreen);

        auto labelResolution = tgui::Label::create("Resolution");
        gridVideo->addWidget(labelResolution, 1, 0);
        gridVideo->add(labelResolution);
        data.ui.menuSettings.comboBoxResolution = tgui::ComboBox::create();
        gridVideo->addWidget(data.ui.menuSettings.comboBoxResolution, 1, 1);
        gridVideo->add(data.ui.menuSettings.comboBoxResolution);

        auto resolutions = sf::VideoMode::getFullscreenModes();

        /// @todo: maybe load every time opened. screen could change
        for (unsigned int i = 0; i < resolutions.size(); ++i)
        {
            if (resolutions[i].bitsPerPixel == 32)
                data.ui.menuSettings.comboBoxResolution->addItem(fmt::format("{}x{}", resolutions[i].width, resolutions[i].height), std::to_string(i));
        }
        data.ui.menuSettings.comboBoxResolution->setSelectedItemById("0");




        auto labelVsync = tgui::Label::create("Vsync");
        gridVideo->addWidget(labelVsync, 2, 0);
        gridVideo->add(labelVsync);
        data.ui.menuSettings.checkBoxVsync = tgui::CheckBox::create();
        gridVideo->addWidget(data.ui.menuSettings.checkBoxVsync, 2, 1);
        gridVideo->add(data.ui.menuSettings.checkBoxVsync);

        /*auto labelMaxFramerate = tgui::Label::create("Max framerate");
        grid->addWidget(labelMaxFramerate, 3, 0);
        grid->add(labelMaxFramerate);
        data.ui.menuSettings.sliderMaxFramerate = tgui::Slider::create(3);
        grid->addWidget(data.ui.menuSettings.sliderMaxFramerate, 3, 1);
        grid->add(data.ui.menuSettings.sliderMaxFramerate);*/

        panelVideo->add(gridVideo);
    }

/// GAMEPLAY



/// HUD
    auto gridHud= tgui::Grid::create();
    //gridHud->setSize({ConstScreenLayout::width, ConstScreenLayout::hudHeight});
    gridHud->setSize({"100%", ConstScreenLayout::hudHeight});

/// TF ????
///////////////////
//    tgui::Button::Ptr ppp = tgui::Button::create("fff");
//    ppp->setSize(ConstScreenLayout::uiWidth, ConstScreenLayout::hudHeight);
//    data.ui.tgui->add(ppp);
///////////////////


    data.ui.menuHud.labelMoney = tgui::Label::create();
    //data.ui.menuHud.labelMoney->setPosition({"&.width / 2 - width / 2", "10"});
    //data.ui.menuHud.labelMoney->setSize({500, 50});
    data.ui.menuHud.labelMoney->setTextSize(20);
    //data.ui.tgui->add(data.ui.menuHud.labelMoney);
    gridHud->addWidget(data.ui.menuHud.labelMoney, 0, 1);
    gridHud->add(data.ui.menuHud.labelMoney);

    data.ui.menuHud.buttonBuyBall = tgui::Button::create();
    gridHud->addWidget(data.ui.menuHud.buttonBuyBall, 0, 0);
    gridHud->add(data.ui.menuHud.buttonBuyBall);

    data.ui.menuHud.buttonBuySplatter = tgui::Button::create();
    gridHud->addWidget(data.ui.menuHud.buttonBuySplatter, 1, 0);
    gridHud->add(data.ui.menuHud.buttonBuySplatter);

    data.ui.menuHud.buttonBuySniper = tgui::Button::create();
    gridHud->addWidget(data.ui.menuHud.buttonBuySniper, 2, 0);
    gridHud->add(data.ui.menuHud.buttonBuySniper);

    data.ui.menuHud.buttonSettings = tgui::Button::create("sett");
    //data.ui.menuHud.buttonSettings->setPosition(ConstRendering::viewWidth - 55, 5);
    //data.ui.menuHud.buttonSettings->setSize(50, 50);
    data.ui.menuHud.buttonSettings->setText("Sett");
    //data.ui.tgui->add(data.ui.menuHud.buttonSettings);
    gridHud->addWidget(data.ui.menuHud.buttonSettings, 0, 2);
    gridHud->add(data.ui.menuHud.buttonSettings);


    data.ui.tgui->add(gridHud);

    data.ui.menuHud.buttonSettings->connect("pressed", [&]() {
        data.ui.menuSettings.window->setVisible(!data.ui.menuSettings.window->isVisible());
    });


/// add windows in front of everything else
    data.ui.tgui->add(data.ui.menuSettings.window);

}

}
